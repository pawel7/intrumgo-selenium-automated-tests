Feature: Database manipulation Script

  @database
  Scenario: Add users to IP table
    Given I navigate to SE country page
    And I insert test users in database using CSV

  @database
  Scenario: Add cases to CLIENT_CASE table
    Given I navigate to SE country page
    And I insert test client cases in database using CSV

  @database
  Scenario: Add obligations to OBLIGATIONS table
    Given I navigate to SE country page
    And I insert test obligations in database using CSV

  @database
  Scenario: Add case financial summary to CLIENT_CASE_FINANCIAL_SUMMARY table
    Given I navigate to SE country page
    And i insert financial data in database using CSV

#  @database
#  Scenario: Add static cases/obligations/CLIENTCASE_FINANCIAL_SUMMARY
#        Given I insert static cases and other items

#  @database
#  Scenario: Delete empty JSONS
#    Given I delete empty JSONS