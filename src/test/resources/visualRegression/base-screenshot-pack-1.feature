Feature: Visual Regression suite

  @base
  Scenario: BASE-Collect screen of login page for se
    Given I navigate to se country page
    And I take base screenshot for login_page_SE page

  @base
  Scenario: BASE-Collect screen of login page for no
    Given I navigate to no country page
    When User changes language to - Norsk
    And I take base screenshot for login_page_no page

  @base
  Scenario: BASE-Collect screen of login page for se in english
    Given I navigate to se country page
    When User changes language to - English
    And I take base screenshot for login_page_se_eng page

  @base
  Scenario: BASE-Collect screen of login page for no in english
    Given I navigate to no country page
    When User changes language to - English
    And I take base screenshot for login_page_no_eng page

  @base
  Scenario: BASE-Collect screen of login with credential from claim letter
    Given I navigate to se country page
    When User changes language to - Svenska
    And User selects simple auth login
    And I take base screenshot for login_Credentials_claim_letter_page_se page

  @base
  Scenario: BASE-Collect screen of login with credential from claim letter in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    And I take base screenshot for login_Credentials_claim_letter_page_se_eng page

  @base
  Scenario: BASE-Collect screen of login with credential from claim letter filled in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And I wait for 1 seconds
    And I take base screenshot for login_Credentials_claim_letter_page_se_eng page

  @base
  Scenario: BASE-Collect screen of 1st payment page
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 2 seconds
    When User clicks pay now button
    And I take base screenshot for 1st_payment_page_se page

  @base
  Scenario: BASE-Collect screen of 1st payment page in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 1 seconds
    When User clicks pay now button
    And I take base screenshot for 1st_payment_page_se_eng page

  @base
  Scenario: BASE-Collect screen of 2nd payment page
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 1 seconds
    When User clicks pay now button
    And User clicks next button on payment page
    And I take base screenshot for 2nd_payment_page_se page

  @base
  Scenario: BASE-Collect screen of 2nd payment page in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 1 seconds
    When User clicks pay now button
    And User clicks next button on payment page
    And I take base screenshot for 2nd_payment_page_se_eng page

  @base
  Scenario: BASE-Collect screen of 2nd payment page expanded bank transfer
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 1 seconds
    When User clicks pay now button
    And User clicks next button on payment page
    And user clicks on bank transfer component
    And I take base screenshot for 2nd_payment_page_exp_se page

  @base
  Scenario: BASE-Collect screen of 2nd payment page in english expanded bank transfer
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 1 seconds
    When User clicks pay now button
    And User clicks next button on payment page
    And user clicks on bank transfer component
    And I take base screenshot for 2nd_payment_page_se_exp_eng page

  @base
  Scenario: BASE-Collect screen of 3rd payment page
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 1 seconds
    When User clicks pay now button
    And User clicks next button on payment page
    And user selects fourth bank radio button
    And User clicks Pay with button
    And Result payment page is displayed
    And I take base screenshot for 3rd_payment_page_se page

  @base
  Scenario: BASE-Collect screen of 3rd payment page in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 1 seconds
    When User clicks pay now button
    And User clicks next button on payment page
    And user selects fourth bank radio button
    And User clicks Pay with button
    And Result payment page is displayed
    And I take base screenshot for 3rd_payment_page_se_eng page

  @base
  Scenario: BASE-Collect screen of closed cases - no cases SE
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 2 seconds
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_no_cases_SE page

  @base
  Scenario: BASE-Collect screen of closed cases - no cases SE english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 2 seconds
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_no_cases_SE_eng page

  @base
  Scenario: BASE-Collect screen of closed cases - no cases NO
    Given I navigate to no country page
    When User changes language to - Norsk
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_no_cases_NO page

  @base
  Scenario: BASE-Collect screen of closed cases - no cases NO english
    Given I navigate to no country page
    When User changes language to - English
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_no_cases_NO_eng page

  @base
  Scenario: BASE-Collect screen of closed cases - one case SE
    Given I navigate to se country page
    When User changes language to - Svenska
    When User clicks mock Login button for Sweden
    And User enters oneCaseClosed5 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_one_case_SE page

  @base
  Scenario: BASE-Collect screen of closed cases - one case SE english
    Given I navigate to se country page
    When User changes language to - English
    When User clicks mock Login button for Sweden
    And User enters oneCaseClosed5 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_one_case_SE_eng page

  @base
  Scenario: BASE-Collect screen of closed cases - two cases NO
    Given I navigate to no country page
    When User changes language to - Norsk
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 06028508644 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_two_cases_NO page

  @base
  Scenario: BASE-Collect screen of closed cases - two cases NO english
    Given I navigate to no country page
    When User changes language to - English
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 06028508644 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_two_cases_NO_eng page

  @base
  Scenario: BASE-Collect screen of no open cases SE
    Given I navigate to se country page
    When User changes language to - Svenska
    When User clicks mock Login button for Sweden
    And User enters no_cases_ssn_se to SSN field and click Submit
    And I wait for 2 seconds
    And I take base screenshot for no_open_cases_SE page

  @base
  Scenario: BASE-Collect screen of no open cases SE english
    Given I navigate to se country page
    When User changes language to - English
    When User clicks mock Login button for Sweden
    And User enters no_cases_ssn_se to SSN field and click Submit
    And I wait for 2 seconds
    And I take base screenshot for no_open_cases_SE_eng page

  @base
  Scenario: BASE-Collect screen of no open cases NO
    Given I navigate to no country page
    When User changes language to - Norsk
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And I wait for 2 seconds
    And I take base screenshot for no_open_cases_NO page

  @base
  Scenario: BASE-Collect screen of no open cases NO english
    Given I navigate to no country page
    When User changes language to - English
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And I wait for 2 seconds
    And I take base screenshot for no_open_cases_eng page

  @base
  Scenario: BASE-Collect screen of hamburger menu SE
    Given I navigate to se country page
    When User changes language to - Svenska
    When User clicks mock Login button for Sweden
    And User enters no_cases_ssn_se to SSN field and click Submit
    And User clicks desktop hamburger menu
    And I wait for 2 seconds
    And I take base screenshot for hamburger_menu_SE page

  @base
  Scenario: BASE-Collect screen of hamburger menu SE english
    Given I navigate to se country page
    When User changes language to - English
    When User clicks mock Login button for Sweden
    And User enters no_cases_ssn_se to SSN field and click Submit
    And User clicks desktop hamburger menu
    And I wait for 2 seconds
    And I take base screenshot for hamburger_menu_SE_eng page

  @base
  Scenario: BASE-Collect screen of hamburger menu NO
    Given I navigate to no country page
    When User changes language to - Norsk
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And I wait for 2 seconds
    And I take base screenshot for hamburger_menu_NO page

  @base
  Scenario: BASE-Collect screen of hamburger menu NO english
    Given I navigate to no country page
    When User changes language to - English
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And I wait for 2 seconds
    And I take base screenshot for hamburger_menu_eng page

    #mobile
  @base
  Scenario: BASE-Collect mobile screen of login page for se
    Given I navigate to se country page
    When User changes language to - Svenska
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for login_page_SE page

  @base
  Scenario: BASE-Collect mobile screen of login page for no
    Given I navigate to no country page
    When User changes language to - Norsk
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for login_page_no page

  @base
  Scenario: BASE-Collect mobile screen of login page for se in english
    Given I navigate to se country page
    When User changes language to - English
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for login_page_se_eng page

  @base
  Scenario: BASE-Collect mobile screen of login page for no in english
    Given I navigate to no country page
    When User changes language to - English
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for login_page_no_eng page

  @base
  Scenario: BASE-Collect mobile screen of login with credential from claim letter
    Given I navigate to se country page
    When User changes language to - Svenska
    And User selects simple auth login
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for login_Credentials_claim_letter_page_se page

  @base
  Scenario: BASE-Collect mobile screen of login with credential from claim letter filled in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for login_Credentials_claim_letter_page_se_eng page

  @base
  Scenario: BASE-Collect mobile screen of login with credential from claim letter in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for login_Credentials_claim_letter_page_se_eng page

  @base
  Scenario: BASE-Collect mobile screen of 1st payment page
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 1 seconds
    When User clicks pay now button
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for 1st_payment_page_se page

  @base
  Scenario: BASE-Collect mobile screen of 1st payment page in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    When User clicks pay now button
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for 1st_payment_page_se_eng page

  @base
  Scenario: BASE-Collect mobile screen of 2nd payment page
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    When User clicks pay now button
    And User clicks next button on payment page
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for 2nd_payment_page_se page

  @base
  Scenario: BASE-Collect mobile screen of 2nd payment page in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    When User clicks pay now button
    And User clicks next button on payment page
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for 2nd_payment_page_se_eng page

  @base
  Scenario: BASE-Collect mobile screen of 2nd payment page expanded bank transfer
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    When User clicks pay now button
    And User clicks next button on payment page
    And user clicks on bank transfer component
    And I use mobile screen size
    And I wait for 1 seconds
    And I take base screenshot for 2nd_payment_page_exp_se page

  @base
  Scenario: BASE-Collect mobile screen of 2nd payment page in english expanded bank transfer
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    When User clicks pay now button
    And User clicks next button on payment page
    And user clicks on bank transfer component
    And I use mobile screen size
    And I take base screenshot for 2nd_payment_page_se_exp_eng page

  @base
  Scenario: BASE-Collect mobile screen of 3rd payment page
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    When User clicks pay now button
    And User clicks next button on payment page
    And user selects fourth bank radio button
    And User clicks Pay with button
    And Result payment page is displayed
    And I use mobile screen size
    And I take base screenshot for 3rd_payment_page_se page

  @base
  Scenario: BASE-Collect mobile screen of 3rd payment page in english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    When User clicks pay now button
    And User clicks next button on payment page
    And user selects fourth bank radio button
    And User clicks Pay with button
    And Result payment page is displayed
    And I use mobile screen size
    And I take base screenshot for 3rd_payment_page_se_eng page

  @base
  Scenario: BASE-Collect mobile screen of closed cases - no cases SE
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - Svenska
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And I wait for 3 seconds
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_no_cases_SE page

  @base
  Scenario: BASE-Collect mobile screen of closed cases - no cases SE english
    Given I navigate to se country page
    And User selects simple auth login
    When User changes language to - English
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_no_cases_SE_eng page

  @base
  Scenario: BASE-Collect mobile screen of closed cases - no cases NO
    Given I navigate to no country page
    When User changes language to - Norsk
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_no_cases_NO page

  @base
  Scenario: BASE-Collect mobile screen of closed cases - no cases NO english
    Given I navigate to no country page
    When User changes language to - English
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_no_cases_NO_eng page

  @base
  Scenario: BASE-Collect mobile screen of closed cases - one case SE
    Given I navigate to se country page
    When User changes language to - Svenska
    When User clicks mock Login button for Sweden
    And User enters oneCaseClosed5 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_one_case_SE page

  @base
  Scenario: BASE-Collect mobile screen of closed cases - one case SE english
    Given I navigate to se country page
    When User changes language to - English
    When User clicks mock Login button for Sweden
    And User enters oneCaseClosed5 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_one_case_SE_eng page

  @base
  Scenario: BASE-Collect mobile screen of closed cases - two cases NO
    Given I navigate to no country page
    When User changes language to - Norsk
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 06028508644 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_two_cases_NO page

  @base
  Scenario: BASE-Collect mobile screen of closed cases - two cases NO english
    Given I navigate to no country page
    When User changes language to - English
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 06028508644 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And User clicks '@/hamburgerMenu/label/closedCasesButton' option from hamburger menu on main page
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for closed_cases_two_cases_NO_eng page

  @base
  Scenario: BASE-Collect mobile screen of no open cases SE
    Given I navigate to se country page
    When User changes language to - Svenska
    When User clicks mock Login button for Sweden
    And User enters no_cases_ssn_se to SSN field and click Submit
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for no_open_cases_SE page

  @base
  Scenario: BASE-Collect mobile screen of no open cases SE english
    Given I navigate to se country page
    When User changes language to - English
    When User clicks mock Login button for Sweden
    And User enters no_cases_ssn_se to SSN field and click Submit
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for no_open_cases_SE_eng page

  @base
  Scenario: BASE-Collect mobile screen of no open cases NO
    Given I navigate to no country page
    When User changes language to - Norsk
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for no_open_cases_NO page

  @base
  Scenario: BASE-Collect mobile screen of no open cases NO english
    Given I navigate to no country page
    When User changes language to - English
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for no_open_cases_eng page

  @base
  Scenario: BASE-Collect mobile screen of hamburger menu SE
    Given I navigate to se country page
    When User changes language to - Svenska
    When User clicks mock Login button for Sweden
    And User enters no_cases_ssn_se to SSN field and click Submit
    And User clicks desktop hamburger menu
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for hamburger_menu_SE page

  @base
  Scenario: BASE-Collect mobile screen of hamburger menu SE english
    Given I navigate to se country page
    When User changes language to - English
    When User clicks mock Login button for Sweden
    And User enters no_cases_ssn_se to SSN field and click Submit
    And User clicks desktop hamburger menu
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for hamburger_menu_SE_eng page

  @base
  Scenario: BASE-Collect mobile screen of hamburger menu NO
    Given I navigate to no country page
    When User changes language to - Norsk
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for hamburger_menu_NO page

  @base
  Scenario: BASE-Collect mobile screen of hamburger menu NO english
    Given I navigate to no country page
    When User changes language to - English
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 31125518012 to SSN field and click Submit
    And User clicks desktop hamburger menu
    And I use mobile screen size
    And I wait for 2 seconds
    And I take base screenshot for hamburger_menu_eng page

  @base
  Scenario: BASE-Case details - top view NO
    Given I navigate to no country page
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 06087714991 to SSN field and click Submit
    Then User is transferred to the restricted part
    And User selects the 'English' language from hamburger menu on main page
    And I wait for 1 seconds
    And User opens case number: 32879163
    And I wait for 1 seconds
    And I take base screenshot for case_details_top page

  @base
  Scenario: BASE-Case details - case info first dropdown NO
    Given I navigate to no country page
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 06087714991 to SSN field and click Submit
    Then User is transferred to the restricted part
    And User selects the 'English' language from hamburger menu on main page
    And I wait for 1 seconds
    And User opens case number: 32879163
    And I wait for 1 seconds
    And User expands Case info dropdown on case detail page
    And I take base screenshot for case_details_case_info page

  @base
  Scenario: BASE-Case details - case info second dropdown NO
    Given I navigate to no country page
    When User clicks mock Login button for Norway
    And User select NO text from dropdown Country code
    And User enters 06087714991 to SSN field and click Submit
    Then User is transferred to the restricted part
    And User selects the 'English' language from hamburger menu on main page
    And I wait for 1 seconds
    And User opens case number: 32879163
    And I wait for 1 seconds
    And User expands Payment details dropdown on case detail page
    And I take base screenshot for case_details_case_details page

  @base
  Scenario: BASE-Case details - top view SE
    Given I navigate to se country page
    When User selects simple auth login
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    Then User is transferred to the restricted part
    And User selects the 'English' language from hamburger menu on main page
    And I wait for 1 seconds
    And -param- User opens case number: 1
    And I take base screenshot for case_details_top page

  @base
  Scenario: BASE-Case details - case info first dropdown SE
    Given I navigate to se country page
    When User selects simple auth login
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    Then User is transferred to the restricted part
    And User selects the 'English' language from hamburger menu on main page
    And I wait for 1 seconds
    And -param- User opens case number: 1
    And User expands Case info dropdown on case detail page
    And I take base screenshot for case_details_case_info page

  @base
  Scenario: BASE-Case details - case info second dropdown NO
    Given I navigate to se country page
    When User selects simple auth login
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    Then User is transferred to the restricted part
    And User selects the 'English' language from hamburger menu on main page
    And I wait for 1 seconds
    And -param- User opens case number: 1
    And User expands Case concern dropdown on case detail page
    And I take base screenshot for case_details_case_concern page

  @base
  Scenario: BASE-Case details - case info third dropdown NO
    Given I navigate to se country page
    When User selects simple auth login
    When User enters automation.se.user.1 to username field
    And User enters Password1 to password field
    And User clicks Login button
    Then User is transferred to the restricted part
    And User selects the 'English' language from hamburger menu on main page
    And I wait for 1 seconds
    And -param- User opens case number: 1
    And User expands Payment details dropdown on case detail page
    And I take base screenshot for case_details_payment_details page


