Feature: IntrumGo Clients tests

  @JIRA-NOM-XX
  Scenario: User is able to see Pending clients
    Given I navigate to IntrumGo page
    Then User sees project url ending with 'clients/pending'
    And User sees Intrum icon
    And User sees 100 clients with status PENDING
    And User clicks Pending clients
    And User sees 100 clients with status PENDING
    And User sees following table titles for clients section
      | COLUMN1 | COLUMN2       | COLUMN3       | COLUMN4     | COLUMN5 | COLUMN6     | COLUMN7 | COLUMN8      |
      | Id      | Company name  | Bank Account  | Business id | Client  | Client tel  | Status  | Client email |
    And User sees Clients filters
      | CLIENTS | PENDING | REJECTED | APPROVED |
      | Clients | Pending | Rejected | Approved |
    And User sees Cases filters
      | CASES | PENDING | REJECTED | APPROVED |
      | Cases | Pending | Rejected | Approved |


  Scenario: User is able to see Rejected clients
    Given I navigate to IntrumGo page
    And User clicks Rejected clients
    Then User sees project url ending with 'clients/rejected'
    And User sees Intrum icon
    And User sees 100 clients with status REJECTED
    And User sees following table titles for clients section
      | COLUMN1 | COLUMN2       | COLUMN3       | COLUMN4     | COLUMN5 | COLUMN6     | COLUMN7 | COLUMN8      |
      | Id      | Company name  | Bank Account  | Business id | Client  | Client tel  | Status  | Client email |
    And User sees Clients filters
      | CLIENTS | PENDING | REJECTED | APPROVED |
      | Clients | Pending | Rejected | Approved |
    And User sees Cases filters
      | CASES | PENDING | REJECTED | APPROVED |
      | Cases | Pending | Rejected | Approved |

  Scenario: User is able to see Approved clients
    Given I navigate to IntrumGo page
    And User clicks Approved clients
    Then User sees project url ending with 'clients/activated'
    And User sees Intrum icon
    And User sees 100 clients with status APPROVED
    And User sees following table titles for clients section
      | COLUMN1 | COLUMN2       | COLUMN3       | COLUMN4     | COLUMN5 | COLUMN6     | COLUMN7 | COLUMN8      |
      | Id      | Company name  | Bank Account  | Business id | Client  | Client tel  | Status  | Client email |
    And User sees Clients filters
      | CLIENTS | PENDING | REJECTED | APPROVED |
      | Clients | Pending | Rejected | Approved |
    And User sees Cases filters
      | CASES | PENDING | REJECTED | APPROVED |
      | Cases | Pending | Rejected | Approved |

  Scenario: User is able to see Pending pages with pagination
    Given I navigate to IntrumGo page
    And User clicks Pending clients
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 1       | Mita          | 4142198750  | 06-746-4962         | Janette Norbury     | 7145861424   | jnorbury0@seesaa.net         |
    And User sees following client in row: 2
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 2       | Agivu         | 4913309390  | 69-580-0134         | Benoite Treslove    | 6274840402   | btreslove1@deliciousdays.com |
    And User sees following client in row: 3
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 3       | Ailane        | 9685067813  | 36-251-8405         | Daisey Jeafferson   | 5802505451   | djeafferson2@people.com.cn   |
    And User sees following client in row: 98
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 191     | Meedoo        | 9657539277  | 12-721-6949         | Sheri Plumley       | 6546501655   | splumley5a@jiathis.com       |
    And User sees following client in row: 99
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 194     | Yakidoo       | 3384983068  | 43-218-9364         | Lacie Edridge       | 7359008061   | ledridge5d@mlb.com           |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 196     | Gabtune       | 0574642579  | 93-584-7423         | Mimi Ridde          | 9091544589   | mridde5f@buzzfeed.com        |
    And User clicks next page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 197     | Abata         | 6614074253  | 61-275-4990         | Nalani Henighan     | 7148805423   | nhenighan5g@bravesites.com   |
    And User sees following client in row: 2
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 202     | Skilith       | 3549492456  | 96-874-5089         | Kylynn Statefield   | 2997149208   | kstatefield5l@ycombinator.com|
    And User sees following client in row: 3
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 204     | Thoughtstorm  | 7009592179  | 27-757-0492         | Cacilia Caesmans    | 4602752480   | ccaesmans5n@marketwatch.com  |
    And User sees following client in row: 98
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 365     | Plajo         | 1884301185  | 81-076-1373         | Gae Shivell         | 8136542989   | gshivella4@ycombinator.com   |
    And User sees following client in row: 99
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 369     | Cogidoo       | 8978704034  | 28-713-2089         | Calida Brauns       | 9178389112   | cbraunsa8@networksolutions.com|
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 373     | Innotype      | 1658943139  | 60-357-3640         | Jacynth Pellitt     | 6442632660   | jpellittac@livejournal.com   |
    And User clicks prev page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 1       | Mita          | 4142198750  | 06-746-4962         | Janette Norbury     | 7145861424   | jnorbury0@seesaa.net         |
    And User sees following client in row: 2
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 2       | Agivu         | 4913309390  | 69-580-0134         | Benoite Treslove    | 6274840402   | btreslove1@deliciousdays.com |
    And User sees following client in row: 3
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 3       | Ailane        | 9685067813  | 36-251-8405         | Daisey Jeafferson   | 5802505451   | djeafferson2@people.com.cn   |
    And User sees following client in row: 98
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 191     | Meedoo        | 9657539277  | 12-721-6949         | Sheri Plumley       | 6546501655   | splumley5a@jiathis.com       |
    And User sees following client in row: 99
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 194     | Yakidoo       | 3384983068  | 43-218-9364         | Lacie Edridge       | 7359008061   | ledridge5d@mlb.com           |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 196     | Gabtune       | 0574642579  | 93-584-7423         | Mimi Ridde          | 9091544589   | mridde5f@buzzfeed.com        |
    And User clicks next page
    And User clicks next page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 375     |  Dabjam       | 1667479474  | 36-020-8277         | Kizzee Cuss         | 2248058818   | kcussae@washington.edu       |
    And User clicks next page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 585     | Twimm         | 0108396649  | 80-095-1313         | Gianna Zottoli      | 2724699906   | gzottolig8@ucoz.ru           |
    And User clicks next page


  Scenario: User is able to see Rejected pages with pagination
    Given I navigate to IntrumGo page
    And User clicks Rejected clients
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 4       | Demivee       | 1726137317  | 19-877-0395         | Jobey McColm        | 2918787670   |  jmccolm3@paginegialle.it    |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 540     | Npath         | 7533567234  | 54-401-8664         | Martha Larkin       | 1453373748   | mlarkinez@shop-pro.jp        |
    And User clicks next page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 542     | Yamia         | 9619943945  | 37-866-4116         | Paulette Bicknell   | 1844668465   | pbicknellf1@drupal.org       |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 1051    |  Eabox        | 8691155949  | 99-018-0280         | Enrika Flett        | 6934978727   | eflett1e@google.com          |
    And User clicks next page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 1052   | Fivespan      | 7401017644  | 60-177-9441         | Rosmunda Merrett    | 9383469474   | rmerrett1f@yolasite.com      |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 1577    | Browseblab    | 9566841825  | 55-501-0513         | Van Adie            | 1988682320   | vadieg0@dion.ne.jp           |
    And User clicks prev page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 542     | Yamia         | 9619943945  | 37-866-4116         | Paulette Bicknell   | 1844668465   | pbicknellf1@drupal.org       |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 1051    |  Eabox        | 8691155949  | 99-018-0280         | Enrika Flett        | 6934978727   | eflett1e@google.com          |
    And User clicks prev page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 4       | Demivee       | 1726137317  | 19-877-0395         | Jobey McColm        | 2918787670   |  jmccolm3@paginegialle.it    |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 540     | Npath         | 7533567234  | 54-401-8664         | Martha Larkin       | 1453373748   | mlarkinez@shop-pro.jp        |


  Scenario: User is able to see Approved pages with pagination
    Given I navigate to IntrumGo page
    And User clicks Approved clients
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 7       | Livefish      | 0824963423  | 07-451-8316         | Carlin Ashbolt      | 8925134316   | cashbolt6@senate.gov         |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 361     | Thoughtworks  | 7253950534  | 42-085-8856         | Karlene Virgo       | 6743511735   | kvirgoa0@360.cn              |
    And User clicks next page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 363     | Skinder       | 4587273600  | 68-232-2181         | Gert Leathe         | 7159414057   | gleathea2@topsy.com          |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 659     | Pixonyx       | 3494229864  | 43-086-0922         | Francyne Stallen    | 1347926028   | fstallenia@globo.com         |
    And User clicks next page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 662     | Skippad       | 5798582094  | 47-589-4475         | Henrieta Dysart     | 6517264610   | hdysartid@mediafire.com      |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 956     | Feedspan      | 9424848447  | 44-268-3348         | Lucina Waterworth   | 5189336201   | lwaterworthqj@wordpress.org  |
    And User clicks prev page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 363     | Skinder       | 4587273600  | 68-232-2181         | Gert Leathe         | 7159414057   | gleathea2@topsy.com          |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 659     | Pixonyx       | 3494229864  | 43-086-0922         | Francyne Stallen    | 1347926028   | fstallenia@globo.com         |
    And User clicks prev page
    And User sees following client in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 7       | Livefish      | 0824963423  | 07-451-8316         | Carlin Ashbolt      | 8925134316   | cashbolt6@senate.gov         |
    And User sees following client in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                      |
      | 361     | Thoughtworks  | 7253950534  | 42-085-8856         | Karlene Virgo       | 6743511735   | kvirgoa0@360.cn              |