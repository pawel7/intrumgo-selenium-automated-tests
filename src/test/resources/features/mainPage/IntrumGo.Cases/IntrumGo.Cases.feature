Feature: IntrumGo Cases tests

  @JIRA-NOM-XX
  Scenario: User is able to see Pending clients
    Given I navigate to IntrumGo page
    And User clicks Pending cases
    Then User sees project url ending with 'cases/pending'
    And User sees Intrum icon
    And User sees 100 cases with status PENDING
    And User sees following table titles for cases section
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6     | COLUMN7 | COLUMN8      |
      | Id      | Company name  | Deptor City | Deptor Business id  | Deptor company name | Deptor Name | Status  | Deptor Email |
    And User sees Clients filters
      | CLIENTS | PENDING | REJECTED | APPROVED |
      | Clients | Pending | Rejected | Approved |
    And User sees Cases filters
      | CASES | PENDING | REJECTED | APPROVED |
      | Cases | Pending | Rejected | Approved |


  Scenario: User is able to see Rejected clients
    Given I navigate to IntrumGo page
    And User clicks Rejected cases
    Then User sees project url ending with 'cases/rejected'
    And User sees Intrum icon
    And User sees 100 cases with status REJECTED
    And User sees following table titles for cases section
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6     | COLUMN7 | COLUMN8      |
      | Id      | Company name  | Deptor City | Deptor Business id  | Deptor company name | Deptor Name | Status  | Deptor Email |
    And User sees Clients filters
      | CLIENTS | PENDING | REJECTED | APPROVED |
      | Clients | Pending | Rejected | Approved |
    And User sees Cases filters
      | CASES | PENDING | REJECTED | APPROVED |
      | Cases | Pending | Rejected | Approved |

  Scenario: User is able to see Approved clients
    Given I navigate to IntrumGo page
    And User clicks Approved cases
    Then User sees project url ending with 'cases/activated'
    And User sees Intrum icon
    And User sees 100 cases with status APPROVED
    And User sees following table titles for cases section
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6     | COLUMN7 | COLUMN8      |
      | Id      | Company name  | Deptor City | Deptor Business id  | Deptor company name | Deptor Name | Status  | Deptor Email |
    And User sees Clients filters
      | CLIENTS | PENDING | REJECTED | APPROVED |
      | Clients | Pending | Rejected | Approved |
    And User sees Cases filters
      | CASES | PENDING | REJECTED | APPROVED |
      | Cases | Pending | Rejected | Approved |

  Scenario: User is able to see Pending pages with pagination
    Given I navigate to IntrumGo page
    And User clicks Pending cases
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 2       |               | St. Louis   | 453158              | BuzzFeed            | Carter Khan  | Carter_Khan8926@atink.com  |
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 269     |               | Madison     | 6065605             | UPC                 | Bob Holt     | Bob_Holt2511@zorer.org     |
    And User clicks next page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 272     |               | Lakewood    | 5964330             | Erickson            | Michelle Rogers| Michelle_Rogers4701@bauros.biz|
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 567     |               | Charlotte   | 5795240             | Leadertech Consulting| Owen Martin | Owen_Martin6180@zorer.org  |
    And User clicks next page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 571     |               | Madrid      | 7353088             | Areon Impex         | Harry Yard   | Harry_Yard9903@brety.org   |
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 882     |               | Sacramento  | 7300089             | Apple Inc.          |Deborah Vaughn| Deborah_Vaughn1198@bulaffy.com|
    And User clicks prev page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 272     |               | Lakewood    | 5964330             | Erickson            | Michelle Rogers| Michelle_Rogers4701@bauros.biz|
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 567     |               | Charlotte   | 5795240             | Leadertech Consulting| Owen Martin | Owen_Martin6180@zorer.org  |
    And User clicks prev page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 2       |               | St. Louis   | 453158              | BuzzFeed            | Carter Khan  | Carter_Khan8926@atink.com  |
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 269     |               | Madison     | 6065605             | UPC                 | Bob Holt     | Bob_Holt2511@zorer.org     |

  Scenario: User is able to see Rejected pages with pagination
    Given I navigate to IntrumGo page
    And User clicks Rejected cases
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 13      |               | Anaheim     | 2483085             | Danone              |Joseph Thornton|Joseph_Thornton8188@bretoux.com   |
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 339     |               | Orlando     | 899793              | Coca-Cola Company   | Eduardo Snow | Eduardo_Snow87@nimogy.biz  |
    And User clicks next page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 345     |               | Baltimore   | 3770090             | It Smart Group      | Chris Slater | Chris_Slater4198@dionrab.com|
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 646     |               | Indianapolis| 9098338             | Zepter              | Sloane West  | Sloane_West91@joiniaa.com  |
    And User clicks next page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 647     |               | Lisbon      | 282118              | ExxonMobil          | Kieth Broomfield| Kieth_Broomfield1835@nimogy.biz |
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 942     |               | Bridgeport  | 8397933             | Vodafone            | Julius Knight| Julius_Knight7239@ubusive.com |
    And User clicks prev page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 345     |               | Baltimore   | 3770090             | It Smart Group      | Chris Slater | Chris_Slater4198@dionrab.com|
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 646     |               | Indianapolis| 9098338             | Zepter              | Sloane West  | Sloane_West91@joiniaa.com  |
    And User clicks prev page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 13      |               | Anaheim     | 2483085             | Danone              |Joseph Thornton|Joseph_Thornton8188@bretoux.com   |
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 339     |               | Orlando     | 899793              | Coca-Cola Company   | Eduardo Snow | Eduardo_Snow87@nimogy.biz  |


  Scenario: User is able to see Approved pages with pagination
    Given I navigate to IntrumGo page
    And User clicks Approved cases
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 1       |               | Lincoln     | 9109360             | Zepter              | Danielle Carpenter | Danielle_Carpenter8831@extex.org |
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 295     |               | Bucharest   | 5774225             | UPC                 | Emmanuelle Wilson | Emmanuelle_Wilson8260@muall.tech |
    And User clicks next page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 296     |               | Stockton    | 3110945             | Amazon.com          | Leilani Dann | Leilani_Dann4122@naiker.biz|
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 593     |               | Bellevue    | 1531200             | CarMax              | Leroy York   | Leroy_York3261@vetan.org   |
    And User clicks next page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 595     |               | Oklahoma City| 8484722            | Boeing              | Aiden Cartwright | Aiden_Cartwright1693@ovock.tech |
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 892     |               | Boston      | 9620843             | BuzzFeed            | Rebecca Reading | Rebecca_Reading9253@bauros.biz |
    And User clicks prev page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 296     |               | Stockton    | 3110945             | Amazon.com          | Leilani Dann | Leilani_Dann4122@naiker.biz|
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 593     |               | Bellevue    | 1531200             | CarMax              | Leroy York   | Leroy_York3261@vetan.org   |
    And User clicks prev page
    And User sees following case in row: 1
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 1       |               | Lincoln     | 9109360             | Zepter              | Danielle Carpenter | Danielle_Carpenter8831@extex.org |
    And User sees following case in row: 100
      | COLUMN1 | COLUMN2       | COLUMN3     | COLUMN4             | COLUMN5             | COLUMN6      | COLUMN8                    |
      | 295     |               | Bucharest   | 5774225             | UPC                 | Emmanuelle Wilson | Emmanuelle_Wilson8260@muall.tech |