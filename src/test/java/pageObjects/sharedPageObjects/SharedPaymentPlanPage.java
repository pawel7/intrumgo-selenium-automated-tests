package pageObjects.sharedPageObjects;

import model.impl.Button;
import model.impl.Label;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.BasePage;

public class SharedPaymentPlanPage extends BasePage {
    public SharedPaymentPlanPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    ///////////////////
    /// UI elements ///
    ///////////////////

    public Label stepper(int stepNumber) {
        return new Label(driver, By.xpath("(//stepper//div//div//div[contains(@class, 'step')])[" + stepNumber + "]"));
    }

    public Label headerTitle() {
        return new Label(driver, By.cssSelector("span.primary-title"));
    }

    public Label headerCustomerName() {
        return new Label(driver, By.xpath("(//div[@class = 'context-header']//span)[2]"));
    }

    public Label headerCaseNumber() {
        return new Label(driver, By.xpath("(//div[@class = 'context-header']//span)[3]"));
    }

    public Button closeButton() {
        return new Button(driver, By.cssSelector(".icon.icon-close"));
    }

    public Label pageTitle() {
        return new Label(driver, By.xpath("//div[@class = 'payment-plan-step-content']//*[self::h2 or self::h3]"));
    }

    public Label pageText() {
        return new Label(driver, By.xpath("//div[@class = 'payment-plan-step-content']//p"));
    }
}
