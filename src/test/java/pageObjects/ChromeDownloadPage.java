package pageObjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChromeDownloadPage extends BasePage {


    public ChromeDownloadPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public String getDownloadFileName() throws Throwable {

        for (int i = 1; i < 60; i++) {
            try {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                String script = "return document.querySelector('downloads-manager').shadowRoot.querySelector('downloads-item').shadowRoot.querySelector('div#content').querySelector('span#name').innerText";

                return (String) js.executeScript(script);
            } catch (Exception e) {
                Thread.sleep(500);
            }
        }
        return "Not found downloaded file";
    }

}
