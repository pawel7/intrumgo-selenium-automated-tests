package pageObjects;

import model.impl.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class MainPage extends BasePage {

    public MainPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    ///////////////////
    /// UI elements ///
    ///////////////////

    public Button requestPaymentPlanButton() {
        return new Button(driver, By.id("payment-plan-button-sm"));
    }

    public Button hamburgerIcon() {
        return new Button(driver, By.cssSelector("header icon.open"));
    }

    public Button caseCard(String caseId) {
        return new Button(driver, By.cssSelector(".case-list div[data-case-number = '" + caseId + "']"));
    }

    public Button payNowButton() {
        return new Button(driver, By.cssSelector("#pay-now-button-sm"));
    }

    public Button intrumLogo() {
        return new Button(driver, By.cssSelector(".page-left-menu .page-left-menu-logo-img"));
    }

    public Button clientsPending() {
        return new Button(driver, By.cssSelector("[ng-reflect-app-tp=section-client]  [ng-reflect-app-tp=link-pending]"));
    }

    public Button clientsRejected() {
        return new Button(driver, By.cssSelector("[ng-reflect-app-tp=section-client]  [ng-reflect-app-tp=link-rejected]"));
    }

    public Button clientsApproved() {
        return new Button(driver, By.cssSelector("[ng-reflect-app-tp=section-client]  [ng-reflect-app-tp=link-activated]"));
    }

    public ElementList clientsList() {
        return new ElementList(driver, By.cssSelector(".clients-list .clients-list-entry"));
    }

    public ElementList casesList() {
        return new ElementList(driver, By.cssSelector(".cases-list .cases-list-entry"));
    }

    public ElementList clientsStatusesListPending() {
        return new ElementList(driver, By.cssSelector("[ng-reflect-app-tp=status-value-pending]"));
    }

    public ElementList clientsStatusesListRejected() {
        return new ElementList(driver, By.cssSelector("[ng-reflect-app-tp=status-value-rejected]"));
    }

    public ElementList clientsStatusesListApproved() {
        return new ElementList(driver, By.cssSelector("[ng-reflect-app-tp=status-value-activated]"));
    }

    public ElementList casesStatusesListPending() {
        return new ElementList(driver, By.cssSelector("[ng-reflect-app-tp=status-valuepending]"));
    }

    public ElementList casesStatusesListRejected() {
        return new ElementList(driver, By.cssSelector("[ng-reflect-app-tp=status-valuerejected]"));
    }

    public ElementList casesStatusesListApproved() {
        return new ElementList(driver, By.cssSelector("[ng-reflect-app-tp=status-valueactivated]"));
    }

    public Label clientRow(String rowNumber) {
        return new Label(driver, By.cssSelector("[ng-reflect-app-tp=client-id-" + rowNumber + "]"));
    }

    public Label casesRow(String rowNumber) {
        return new Label(driver, By.cssSelector("[ng-reflect-app-tp=case-id-" + rowNumber + "]"));
    }

    public Label caseRow(String rowNumber) {
        return new Label(driver, By.cssSelector("[ng-reflect-app-tp=case-id-" + rowNumber + "]"));
    }

    public Label clientsTitleColumnX(String columnNumber) {
        return new Label(driver, By.cssSelector(".clients-list-header div:nth-child(" + columnNumber+ ")"));
    }

    public Label casesTitleColumnX(String columnNumber) {
        return new Label(driver, By.cssSelector(".cases-list-header div:nth-child(" + columnNumber+ ")"));
    }

    public Label clientsStatusesFilterTitle() {
        return new Label(driver, By.cssSelector("[ng-reflect-app-tp=label0]"));
    }

    public Label casesStatusesFilterTitle() {
        return new Label(driver, By.cssSelector("[ng-reflect-app-tp=label1]"));
    }

    public Button casesPending() {
        return new Button(driver, By.cssSelector("[ng-reflect-app-tp=section-case]  [ng-reflect-app-tp=link-pending]"));
    }

    public Button casesRejected() {
        return new Button(driver, By.cssSelector("[ng-reflect-app-tp=section-case]  [ng-reflect-app-tp=link-rejected]"));
    }

    public Button casesApproved() {
        return new Button(driver, By.cssSelector("[ng-reflect-app-tp=section-case]  [ng-reflect-app-tp=link-activated]"));
    }

    public Label casesTitleRowsXColumnY(String rowNumber, String columnNumber) {
        return new Label(driver, By.cssSelector(".cases-list-entry:nth-child(" + rowNumber+ ")" + " .cases-list-entry-col:nth-child(" + columnNumber+ ")"));
    }

    public Label clientsTitleRowsXColumnY(String rowNumber, String columnNumber) {
        return new Label(driver, By.cssSelector(".clients-list-entry:nth-child(" + rowNumber+ ")" + " .clients-list-entry-col:nth-child(" + columnNumber+ ")"));
    }

    public Button nextPage() {
        return new Button(driver, By.cssSelector("[ng-reflect-app-tp=next]"));
    }

    public Button prevPage() {
        return new Button(driver, By.cssSelector("[ng-reflect-app-tp=prev]"));
    }




    ///////////////
    /// Methods ///
    ///////////////

    public void checkIfAllClientsTitleElementsAreVisible() {
        ArrayList<UIElement> listOfElements = new ArrayList<UIElement>();
        listOfElements.add(clientsTitleColumnX("1"));
        listOfElements.add(clientsTitleColumnX("2"));
        listOfElements.add(clientsTitleColumnX("3"));
        listOfElements.add(clientsTitleColumnX("4"));
        listOfElements.add(clientsTitleColumnX("5"));
        listOfElements.add(clientsTitleColumnX("6"));
        listOfElements.add(clientsTitleColumnX("7"));
        listOfElements.add(clientsTitleColumnX("8"));
        AreElementsVisibleNewApproach(listOfElements);
    }

//    public void checkIfElementsAreVisibleOnDesktop() {
//        ArrayList<UIElement> list = new ArrayList<>();
//        list.add(welcomeLabel());
//        list.add(companyLogo());
//        list.add(hamburgerIcon());
//
//        AreElementsVisibleNewApproach(list);
//    }

    ///////////////
    /// Methods ///
    ///////////////



}
