package pageObjects;

import model.impl.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.ReadTestDataFile;

import java.util.ArrayList;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    ///////////////////
    /// UI elements ///
    ///////////////////

    public TextBox SSNInputField() {
        return new TextBox(driver, By.cssSelector("#ssn"));
    }

    public Button loginIdLoginButton() {
        return new Button(driver, By.cssSelector("#login-providers .provider-logonid"));
    }

    public Button bankIdLoginButton() {
        return new Button(driver, By.xpath("//div[contains(@class, 'provider-bankid') and not(contains(@class, 'mobile'))]"));
    }

    public Button bankIdMobileLoginButton() {
        return new Button(driver, By.xpath("//div[contains(@class, 'provider-bankid-mobile')]"));
    }

    public Button mockLoginButton() {
        return new Button(driver, By.cssSelector("#login-providers .provider-mock"));
    }

    public TextBox usernameInputField() {
        return new TextBox(driver, By.cssSelector("#username"));
    }

    public TextBox passwordInputField() {
        return new TextBox(driver, By.cssSelector("#password"));
    }

    public Button submitButton() {
        return new Button(driver, By.cssSelector("#logonForm  button"));
    }

    public Label passwordInputFieldLabel() {
        return new Label(driver, By.xpath("//input[@id = 'password']//following-sibling::label"));
    }

    public Label usernameInputFieldLabel() {
        return new Label(driver, By.xpath("//input[@id = 'username']//following-sibling::label"));
    }

    public Label loginPageErrorLabel(String fieldName) {
        return new Label(driver, By.xpath("//input[@id='" + fieldName + "']//..//div[@class='invalid-feedback']"));
    }

    public Label loginPageErrorFromServerLabel(String fieldName) {
        return new Label(driver, By.xpath("//input[@id='" + fieldName + "']//following-sibling::label[2]"));
    }

    public Label cookieText() {
        return new Label(driver, By.cssSelector(".cookie-text"));
    }

    public Button cookiePolicyLink() {
        return new Button(driver, By.cssSelector("div.cookie-text a"));
    }

    public Button cookiePolicyAcceptButton() {
        return new Button(driver, By.cssSelector("div.cookie-button > a"));
    }

    public Label mainTitle() {
        return new Label(driver, By.cssSelector("#template-content div.country-selection-link + div >h3"));
    }

    public Label mainSubTitle() {
        return new Label(driver, By.cssSelector("#template-content div.country-selection-link + div + div >p"));
    }

    public Label loginWithCredentialsTitle() {
        return new Label(driver, By.cssSelector("div.row.welcome-text>div>h3"));
    }

    public Label loginWithCredentialsSubTitle() {
        return new Label(driver, By.cssSelector("div.row.description-text>div>p"));
    }

    public Button changeLoginAlternativeLink() {
        return new Button(driver, By.cssSelector("#back-link"));
    }

    public Button needHelpToLoginButton() {
        return new Button(driver, By.cssSelector("#need-help-to-login > span"));
    }

    public Label currentLanguageLabel() {
        return new Label(driver, By.cssSelector(".lang-link.active"));
    }

    public Dropdown countryCodeDropdown() {
        return new Dropdown(driver, By.cssSelector("#countryCode"));
    }

    public Button changeCountryLink() {
        return new Button(driver, By.cssSelector("#country-selection"));
    }

    ///////////////
    /// Methods ///
    ///////////////

    public void checkIfAllCookieFieldsAreVisible() {
        ArrayList<UIElement> list = new ArrayList<UIElement>();
        list.add(cookiePolicyAcceptButton());
        list.add(cookieText());
        list.add(cookiePolicyLink());
        AreElementsVisibleNewApproach(list);
    }

    public void waitUntilCookieConsentDisappear() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("cookie-consent")));
    }

    public void navigateToLoginPage(String countryCode) {
        driver.navigate().to(properties.getProperty("project.url") + properties.getProperty("application.port") + "/" + countryCode);
        ReadTestDataFile.mainLanguage = countryCode;
    }

    public void navigateToMainPage() {
        driver.navigate().to(properties.getProperty("project.url"));
    }

    public void checkIfAllSimpleLoginFieldsAreVisible() {
        ArrayList<UIElement> listOfElements = new ArrayList<UIElement>();
        listOfElements.add(usernameInputField());
        listOfElements.add(passwordInputField());
        listOfElements.add(submitButton());
        AreElementsVisibleNewApproach(listOfElements);
    }

}
