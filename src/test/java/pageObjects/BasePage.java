package pageObjects;

import cucumber.api.DataTable;
import lombok.extern.log4j.Log4j;
import model.impl.UIElement;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.CustomExpectedConditions;
import utils.DriverSingleton;
import utils.ReadTestDataFile;
import utils.TestProperties;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@Log4j
public class BasePage {
    public RemoteWebDriver driver;
    public WebDriverWait wait;
    public TestProperties properties;
    public SoftAssertions softly = new SoftAssertions();
    public ReadTestDataFile readTestDataFile = new ReadTestDataFile();

    public BasePage(WebDriver driver, WebDriverWait wait) {
        properties = TestProperties.getInstance();
        this.driver = DriverSingleton.getInstance().getDriver();
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    public void MoveToElement(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }

    public Map<String, String> getKeyValuesOfDataTable(DataTable values) {
        Map<String, String> map = new HashMap<String, String>();
        for (List<String> row : values.raw()) {
            map.put(row.get(0), row.get(1));
        }
        return map;
    }

    //Click Method
    public void click(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
        MoveToElement(element);
        element.click();
    }

    //Write Text
    public void writeText(WebElement element, String text) {
        wait.until(ExpectedConditions.visibilityOf(element));
        MoveToElement(element);
        element.clear();
        element.sendKeys(readTestDataFile.getDataFromFile(text));
    }

    //Check if element(s) are visible
//    ToDo: delete if new function (AreElementsVisibleNewApproach) works
    public boolean AreElementsVisible(WebElement element, WebElement... elements) {
        boolean isDisplayed = false;
        for (WebElement elementIterator : elements) {
            wait.until(ExpectedConditions.visibilityOf(element));
            MoveToElement(element);
            isDisplayed = elementIterator.isDisplayed();
        }
        return isDisplayed;
    }

    public void AreElementsVisibleNewApproach(ArrayList<UIElement> listOfElements) {
        for (UIElement element : listOfElements) {
            softly.assertThat(element.isDisplayed()).isTrue();
        }
        softly.assertAll();
    }

    public void ManualWait(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void waitUntilPageIsLoaded() {
        wait.until(CustomExpectedConditions.pageIsLoaded());
    }

    public void assertThatEquals(String actual, String expected) {
        assertThat(actual).isEqualTo(readTestDataFile.getDataFromFile(expected));
    }

    public void assertThatEquals(String actual, String expected, String exceptionMessage) {
        assertThat(actual).as(exceptionMessage).isEqualTo(readTestDataFile.getDataFromFile(expected));
    }

    public void assertThatContains(String actual, String expected, String exceptionMessage) {
        assertThat(actual).as(exceptionMessage).contains(readTestDataFile.getDataFromFile(expected));
    }

    public void assertThatEquals(int actual, int expected, String exceptionMessage) {
        assertThatEquals(String.valueOf(actual), String.valueOf(expected), exceptionMessage);
    }

    public Date convertStringToDate(String date, String format) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date result = formatter.parse(date);
        return result;
    }

    public void switchBrowserTab(int browserTabNumber) {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(browserTabNumber - 1));
    }
}
