package steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import utils.APIService;

import java.io.IOException;

public class httpRequestSteps {

//    SharedContext sharedContext;
    private APIService apiService = new APIService();

//    public httpRequestSteps(SharedContext sharedContext) {
//        this.sharedContext = sharedContext;
//    }

    @Given("^I send delete request Rekondis for (.*)/(.*)$")
    public void iSendDeleteRequestToRekondisForDebtorIDCaseID(String debtorID, String caseID) throws IOException {
        apiService.deletePaymentPlanToRekondisForDebtor(debtorID, caseID);
    }

    @Given("^I send delete request to (.*) for (.*)/(.*)$")
    public void iSendDeleteRequestToNovaPDForDebtorIDCaseID(String novaType, String debtorID, String caseID) throws IOException {
        apiService.deletePaymentPlanToNovaForDebtor(debtorID, caseID, novaType);
    }

    @Given("^Production system sends a message for case (.*) with client (.*) and secret (.*)$")
    public void iPostMessage(String obligationReference, String clientID, String clientSecret) throws IOException {
        apiService.postMessageFromProdSystem(obligationReference, clientID, clientSecret);
    }

//    @Given("^production creates an order with following parameters$")
//    public void productionCreatesAnOrderWithFollowingParameters(DataTable values) throws Throwable {
//        sharedContext.orderID = apiService.postGenericOrderFromProductionSystem(values);
//    }
}
