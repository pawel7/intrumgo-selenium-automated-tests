package steps;

import com.google.gson.Gson;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.DriverSingleton;
import utils.InternalClasses.CustomDate;
import utils.Screenshots;
import utils.TestProperties;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import static utils.DriverSingleton.destroySingletonInstance;

public class BaseSteps {

    protected static Properties properties;

    static {
        properties = TestProperties.getInstance();
    }

    protected WebDriver driver;
    protected WebDriverWait wait;
    ExtentReports extent;
    ExtentTest extentTest;
    String currentDir = System.getProperty("user.dir");
    String featureName;
    String scenarioName;
    public SoftAssertions softly = new SoftAssertions();

    public BaseSteps() {
        try {
            startWebDriver();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void startWebDriver() throws IOException {
        driver = DriverSingleton.getInstance().getDriver();
        wait = new WebDriverWait(driver, 30);
        extent = DriverSingleton.getExtent();
    }

    public void updateExtentTest(String featureName, String scenarioName) {
        extentTest = DriverSingleton.getExtentTest(featureName, scenarioName);
    }

    public void stopWebDriver() {
        driver.quit();
        destroySingletonInstance();
    }

    public void scrollToTheElement(WebElement element) {
        WebElement header1 = driver.findElement(By.cssSelector("div.hero:nth-of-type(1)"));
        WebElement header2 = driver.findElement(By.cssSelector("div.bg-white"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true); window.scrollBy(0, -arguments[1].offsetHeight); window.scrollBy(0, -arguments[2].offsetHeight); ", element, header1, header2);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1);");
    }

    public void scrollToTheElementOnMainPage(WebElement element) {
        WebElement header1 = driver.findElement(By.cssSelector("div.hero:nth-of-type(1)"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true); window.scrollBy(0, -arguments[1].offsetHeight);", element, header1);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1);");
    }

    public void scrollToTheElementOnMainPageOnMobile(WebElement element) {
        WebElement header1 = driver.findElement(By.cssSelector("div.mobile a.primary-button"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true); window.scrollBy(0, -arguments[1].offsetHeight);", element, header1);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1);");
    }


    public void scrollToTheElementOnFAQ(WebElement element) {
        WebElement header1 = driver.findElement(By.cssSelector("div.hero:nth-of-type(1)"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1);");
    }

    public void scrollToTheElementNoHeaders(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true); ", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0, 1);");
    }


    public void takeScreenshotIfFailed(Scenario scenario) {
        try {
            if (scenario.isFailed()) {
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                String currentDir = System.getProperty("user.dir");
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                try {
                    Screenshots screen = new Screenshots();
                    String failedFolder = currentDir + File.separator + properties.getProperty("screenshots.folder1") + File.separator + properties.getProperty("screenshots.folder2") + File.separator + "actual" + File.separator + screen.getNameOfFeature(scenario) + File.separator + screen.getNameOfScenario(scenario) + File.separator + "FAILED" + ".png";
                    screen.createFolders(failedFolder);
                    FileUtils.copyFile(scrFile, new File(failedFolder));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                System.out.println("Screenshots has been captured to: " + currentDir + File.separator + "screenshots" + File.separator + scenario.getName() + " " + timeStamp + ".png");
            }
        } finally {
            //stopWebDriver();
        }
    }

    public String getMinus6MonthsAnd6DaysDateAsString(String pattern) {
        Date dt = new Date();
        LocalDateTime now = LocalDateTime.now().minusMonths(6).minusDays(8);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern).withLocale(Locale.ENGLISH);
        String formatDateTime = now.format(formatter);
        return formatDateTime;
    }

    /**
     * @param date Date paramater should be passed from Feature file in format "###{'pattern':'pattern','day':x,'month':y}"
     *             where pattern is date pattern e.g. dd.MM.YYYY, day is int e.g. -1, 0, 1 and month is int e.g. -1, 0, 1
     * @return Returns specified date as string formatted in pattern provided.
     *
     * <h3>Example: If today is 13th of March 2019 then <b>###{'pattern':'dd.MM.YYYY','day':1,'month':2}</b> would return <b>14.05.2019</b></h3>
     */
    public String getSpecificDate(String date) {
        if (date.startsWith("###")) {
            String passedDate = date.replace("###", "");
            Gson gson = new Gson();
            CustomDate customDate = gson.fromJson(passedDate, CustomDate.class);
            LocalDateTime now = LocalDateTime.now().plusMonths(customDate.month).plusDays(customDate.day);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(customDate.pattern).withLocale(Locale.ENGLISH);
            String formatDateTime = now.format(formatter);
            return formatDateTime;
        }
        return date;
    }

    public String getTodayssDateAsString(String pattern) {
        Date dt = new Date();
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern).withLocale(Locale.ENGLISH);
        String formatDateTime = now.format(formatter);
        return formatDateTime;
    }


    public String getTomorrowsDateAsString(String pattern) {
        Date dt = new Date();
        LocalDateTime now = LocalDateTime.now().plusDays(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern).withLocale(Locale.ENGLISH);
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMMM YYYY").withLocale(Locale.ENGLISH);
        String formatDateTime = now.format(formatter);
        return formatDateTime;
    }

    public String getNovaDate(String pattern) {
        Date dt = new Date();
        LocalDateTime now = LocalDateTime.now().plusMonths(1).plusDays(3);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern).withLocale(Locale.ENGLISH);
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMMM YYYY").withLocale(Locale.ENGLISH);
        String formatDateTime = now.format(formatter);
        return formatDateTime;
    }


    public String getNextMonthDateAsString() {
        Date dt = new Date();
        LocalDateTime now = LocalDateTime.now().plusMonths(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY").withLocale(Locale.ENGLISH);
        String formatDateTime = now.format(formatter);
        return formatDateTime;
    }

    public String getNextWeekDateAsString() {
        Date dt = new Date();
        LocalDateTime now = LocalDateTime.now().plusWeeks(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY").withLocale(Locale.ENGLISH);
        String formatDateTime = now.format(formatter);
        return formatDateTime;
    }

    public void enterValueOrClearInput(By by, String value) {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));

        if (value.equals("<empty>")) {
            WebElement toClear = driver.findElement(by);
            toClear.click();
            toClear.sendKeys(Keys.SPACE);
            toClear.sendKeys(Keys.CONTROL + "a");
            toClear.sendKeys(Keys.DELETE);
            return;
        }

        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(value);

    }

}
