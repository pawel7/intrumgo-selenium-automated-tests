package steps;

import com.relevantcodes.extentreports.LogStatus;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import pageObjects.*;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import utils.Screenshots;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

public class GlobalSteps extends BaseSteps {

    LoginPage loginPage;
    MainPage mainPage;
    BasePage basePage;
//    CaseDetailsPage1 caseDetailPage;
//    PaymentPage1stStep paymentPage;

    String currentDir = System.getProperty("user.dir");
    Scenario scenario;

    @Before
    public void before(Scenario scenario) throws Exception {
        Screenshots screen = new Screenshots();
        scenarioName = screen.getNameOfScenario(scenario);
        featureName = screen.getNameOfFeature(scenario);
        this.scenario = scenario;
        updateExtentTest(featureName, scenarioName);

        loginPage = new LoginPage(driver, wait);
        mainPage = new MainPage(driver, wait);
        basePage = new BasePage(driver, wait);
//        caseDetailPage = new CaseDetailsPage1(driver, wait);
//        paymentPage = new PaymentPage1stStep(driver, wait);
    }

    @After
    public void tearDown(Scenario scenario) throws IOException {
        Screenshots screen = new Screenshots();
        takeScreenshotIfFailed(scenario);
        if (scenario.isFailed()) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            System.out.println("Timestamp is: " + timestamp);
            scenario.write("Timestamp is: " + timestamp);
            extentTest.log(LogStatus.FAIL, "Test failed");
            String failedFolder = currentDir + File.separator + properties.getProperty("screenshots.folder1") + File.separator + properties.getProperty("screenshots.folder2") + File.separator + "actual" + File.separator + screen.getNameOfFeature(scenario) + File.separator + screen.getNameOfScenario(scenario) + File.separator + "FAILED" + ".png";
            extentTest.log(LogStatus.INFO, "Failed Screenshot: " + extentTest.addScreenCapture(failedFolder));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(screen.makeScreenshot(driver).getImage(), "PNG", baos);
            byte[] bytes = baos.toByteArray();
            scenario.embed(bytes, "image/png");


        }
        extent.endTest(extentTest);
        extent.flush();
        stopWebDriver();
        //deleteEmptyJSONS();

    }

    @Given("^I navigate to page (.*)$")
    public void iNavigateToPage(String url) throws Throwable {
        driver.navigate().to(url);
        basePage.waitUntilPageIsLoaded();
        driver.manage().window().maximize();
        extentTest.log(LogStatus.INFO, "Browser is opened and page is loaded");
    }


    @When("^I wait for (.*) seconds")
    public void waitForSeconds(int seconds) {
        basePage.ManualWait(seconds);

        extentTest.log(LogStatus.INFO, "System is delayed for " + seconds + "seconds");

    }

    @Given("^I click ENTER")
    public void clickENTER() {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ENTER).build().perform();
    }

    @And("^I take base screenshot for (.*) page$")
    public void iTakeBaseScreenshot(String pageName) throws Throwable {
        Screenshots screen = new Screenshots();
        String baseFileToSave = currentDir + File.separator + properties.getProperty("screenshots.folder1") + File.separator + properties.getProperty("screenshots.folder2") + File.separator + "base" + File.separator + featureName + File.separator + scenarioName + File.separator + "base-" + pageName + ".png";
        screen.createFolders(baseFileToSave);

        ImageIO.write(screen.makeScreenshot(driver).getImage(), "PNG", new File(baseFileToSave));

        extentTest.log(LogStatus.INFO, "Base screenshot is done");

    }

    @Given("^I navigate to (.*) country page")
    public void navigateToSpecificCountryPage(String countryCode) {
        loginPage.navigateToLoginPage(countryCode);
    }

    @Given("^I navigate to IntrumGo page")
    public void navigateToIntrumGoPage() {
        loginPage.navigateToMainPage();
    }

    @And("^I use mobile screen size")
    public void useMobileScreenSize() {
        driver.manage().window().setSize(new Dimension(400, 720));
        basePage.waitUntilPageIsLoaded();
    }

    @And("^I take actual screenshot for (.*) page$")
    public void iTakeActualScreenshot(String pageName) throws Throwable {
        String commonPartOfScreenshotsPath = currentDir + File.separator + properties.getProperty("screenshots.folder1") + File.separator + properties.getProperty("screenshots.folder2");
        Screenshots screen = new Screenshots();
        String actualFileToSave = commonPartOfScreenshotsPath + File.separator + "actual" + File.separator + featureName + File.separator + scenarioName + File.separator + "actual-" + pageName + ".png";
        screen.createFolders(actualFileToSave);

        extentTest.log(LogStatus.INFO, "Actual screenshot is done");

        ImageIO.write(screen.makeScreenshot(driver).getImage(), "PNG", new File(actualFileToSave));
        System.out.println(commonPartOfScreenshotsPath + File.separator + "base" + File.separator + featureName + File.separator + "base-" + scenarioName + File.separator + "base-" + pageName + ".png");
        BufferedImage baseImage = ImageIO.read(new File(commonPartOfScreenshotsPath + File.separator + "base" + File.separator + featureName + File.separator + "base-" + scenarioName + File.separator + "base-" + pageName + ".png"));
        BufferedImage actualImage = ImageIO.read(new File(actualFileToSave));
        //BufferedImage actualImage = screen.makeScreenshot(driver).getImage();

        ImageDiffer imgDiff = new ImageDiffer();

        extentTest.log(LogStatus.INFO, "Base Screenshot: " + extentTest.addScreenCapture(commonPartOfScreenshotsPath + File.separator + "base" + File.separator + featureName + File.separator + "base-" + scenarioName + File.separator + "base-" + pageName + ".png"));
        extentTest.log(LogStatus.INFO, "Actual Screenshot: " + extentTest.addScreenCapture(commonPartOfScreenshotsPath + File.separator + "actual" + File.separator + featureName + File.separator + scenarioName + File.separator + "actual-" + pageName + ".png"));

        ImageDiff diff = imgDiff.makeDiff(baseImage, actualImage);
        diff.withDiffSizeTrigger(Integer.parseInt(properties.getProperty("number.of.accepted.different.pixels")));

        String transparentMarkedImage = commonPartOfScreenshotsPath + File.separator + "actual" + File.separator + featureName + File.separator + scenarioName + File.separator + "diff1-" + pageName + "_transparent_" + ".png";
        screen.createFolders(transparentMarkedImage);

        ImageIO.write(baseImage, "PNG", new File(commonPartOfScreenshotsPath + File.separator + "actual" + File.separator + featureName + File.separator + scenarioName + File.separator + "base-copy-" + pageName + ".png"));
        ImageIO.write(diff.getTransparentMarkedImage(), "PNG", new File(transparentMarkedImage));
        ImageIO.write(diff.getMarkedImage(), "PNG", new File(commonPartOfScreenshotsPath + File.separator + "actual" + File.separator + featureName + File.separator + scenarioName + File.separator + "diff2-" + pageName + "_markedImage_" + ".png"));

        System.out.println("\nNumber of pixels that differ " + diff.getDiffSize() + "\n");

        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        ImageIO.write(baseImage, "PNG", baos1);
        byte[] bytes1 = baos1.toByteArray();
        scenario.embed(bytes1, "image/png");

        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        ImageIO.write(actualImage, "PNG", baos2);
        byte[] bytes2 = baos2.toByteArray();
        scenario.embed(bytes2, "image/png");

        ByteArrayOutputStream baos3 = new ByteArrayOutputStream();
        ImageIO.write(diff.getTransparentMarkedImage(), "PNG", baos3);
        byte[] bytes3 = baos3.toByteArray();
        scenario.embed(bytes3, "image/png");

        ByteArrayOutputStream baos4 = new ByteArrayOutputStream();
        ImageIO.write(diff.getMarkedImage(), "PNG", baos4);
        byte[] bytes4 = baos4.toByteArray();
        scenario.embed(bytes4, "image/png");

        extentTest.log(LogStatus.INFO, "Diff TransparentMarkedImage Screenshot: " + extentTest.addScreenCapture(commonPartOfScreenshotsPath + File.separator + "actual" + File.separator + featureName + File.separator + scenarioName + File.separator + "diff1-" + pageName + "_transparent_" + ".png"));
        extentTest.log(LogStatus.INFO, "Diff MarkedImage Screenshot: " + extentTest.addScreenCapture(commonPartOfScreenshotsPath + File.separator + "actual" + File.separator + featureName + File.separator + scenarioName + File.separator + "diff2-" + pageName + "_markedImage_" + ".png"));
        extentTest.log(LogStatus.INFO, "Number of pixels that differ " + diff.getDiffSize() + ".  Maximal number of different pixels is: " +
                properties.getProperty("number.of.accepted.different.pixels"));

        Assert.assertFalse("Images " + pageName + " are not the same", diff.hasDiff());
    }

    @Then("^User switches to browser tab '(.*)'$")
    public void userSwitchesToBrowserTab(int browserTabNumber) {
        basePage.switchBrowserTab(browserTabNumber);
    }

    @Then("^User sees project url ending with '(.*)'$")
    public void userSeesThePageUrl(String url) {
        basePage.assertThatEquals(driver.getCurrentUrl(), properties.getProperty("project.url") + url, "Page url is wrong");
    }

}
