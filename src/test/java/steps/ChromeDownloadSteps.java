package steps;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import pageObjects.ChromeDownloadPage;
import pageObjects.MainPage;
import utils.Screenshots;

import static org.assertj.core.api.Assertions.assertThat;

public class ChromeDownloadSteps extends BaseSteps {

    ChromeDownloadPage chromeDownloadPage;
    MainPage mainPage;


    @Before
    public void before(Scenario scenario) throws Exception {
        Screenshots screen = new Screenshots();
        scenarioName = screen.getNameOfScenario(scenario);
        featureName = screen.getNameOfFeature(scenario);
        updateExtentTest(featureName, scenarioName);
        chromeDownloadPage = new ChromeDownloadPage(driver, wait);
        mainPage = new MainPage(driver, wait);
    }

    @And("^user validate chrome download page for download (.*)$")
    public void userValidateChromeDownloadPageForDownload_KDFEPdf(String fileName) throws Throwable {
        assertThat(chromeDownloadPage.getDownloadFileName())
                .as("download File name %s is there", fileName)
                .contains(fileName);
    }

}


