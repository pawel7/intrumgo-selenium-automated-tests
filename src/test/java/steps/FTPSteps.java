package steps;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jcraft.jsch.*;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import steps.databaseSteps.CaseDatabaseSteps;
import utils.TestProperties;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import static org.assertj.core.api.Assertions.assertThat;

public class FTPSteps {

    protected static Properties properties;

    static {
        properties = TestProperties.getInstance();
    }

    JSch client;
    Session session;
    ChannelSftp sftp;


    private String exportFileName;
    private LocalDateTime now;

    @Given("^I connect to export FTP server$")
    public void iConnectToExportFTPServer() throws IOException, JSchException, SftpException {
        client = new JSch();
        session = client.getSession(properties.getProperty("export.ftp.username"), properties.getProperty("export.ftp.hostname"), 22222);
        session.setConfig("StrictHostKeyChecking", "no");
        session.setConfig("PreferredAuthentications", "password");
        session.setPassword(properties.getProperty("export.ftp.password"));
        session.connect();
        sftp = (ChannelSftp) session.openChannel("sftp");
        sftp.connect();
    }

    @And("^I delete all files in path (.*)$")
    public void iDeleteAllFiles(String path) throws IOException, SftpException {
        Vector<ChannelSftp.LsEntry> list = sftp.ls(path);
        for (ChannelSftp.LsEntry entry : list) {
            if (!(entry.getFilename().equals(".") || entry.getFilename().equals(".."))) {
                System.out.println(path + "/" + entry.getFilename());
                sftp.rm(path + "/" + entry.getFilename());
            }
        }
    }

    @And("^I validate export file is there with name and params (.*) in path (.*)$")
    public void iValidateExportFileIsThere(String prodSys, String path) throws IOException, SftpException {
        Vector<ChannelSftp.LsEntry> list = sftp.ls(path);
        for (ChannelSftp.LsEntry entry : list) {
            if (!(entry.getFilename().equals(".") || entry.getFilename().equals(".."))) {
                exportFileName = entry.getFilename();
            }
        }
        now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String formatDateTime = now.format(formatter);
        System.out.println(formatDateTime);

        assertThat(exportFileName).as("Filename Is correct").startsWith("DW" + prodSys + ".dwmsg.");
        assertThat(exportFileName).as("Date is Filename is correct").contains(formatDateTime);
        assertThat(exportFileName.length()).as("Filename should be 33 characters long").isEqualTo(33);

    }

    @And("^I validate export file content with params$")
    public void iValidateExportFileContent(DataTable table) throws IOException, SQLException, SftpException {

        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        String prodSystem = data.get(0).get("prod_system");
        String caseRef = data.get(0).get("case_ref");
        String ocr = data.get(0).get("ocr");
        String caseId = data.get(0).get("case_id");
        String amount = data.get(0).get("amount");
        String debtorRowNum = data.get(0).get("debtor_row_num");
        String paymentProvider = data.get(0).get("payment_provider");
        String ssn = data.get(0).get("ssn");
        String path = data.get(0).get("path");

        InputStream in = sftp.get(path + "/" + exportFileName);
        Reader targetReader = new InputStreamReader(in);
        CSVParser csvParser = new CSVParser(targetReader, CSVFormat.newFormat(';'));


        CaseDatabaseSteps caseDatabaseSteps = new CaseDatabaseSteps();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formatDateTime = now.format(formatter);
        DateTimeFormatter formatterForFile = DateTimeFormatter.ofPattern("yyyyMMdd");
        String formatDateTimeForFile = now.format(formatterForFile);
        List<CSVRecord> records = csvParser.getRecords();

        //Assert 200 record
        CSVRecord record200 = records.get(0);
        System.out.println("SIZE OF RECORD:" + record200.size());
        assertThat(record200.size()).as("Field count is correct").isEqualTo(5);
        assertThat(record200.get(0)).as("Record should start with 200").isEqualTo("200");
        assertThat(record200.get(1)).as("Date is correct").contains(formatDateTime);
        assertThat(record200.get(3)).as("Production system is correct").isEqualTo(prodSystem);
        // Transaction trans = caseDatabaseSteps.getAllTransactionsForCase(caseRef);
        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create();
        // System.out.println(gson.toJson(trans));

        //Assert 211 record
        CSVRecord record211 = records.get(1);


        System.out.println("SIZE OF RECORD:" + record211.size());
        assertThat(record211.size()).as("Field count is correct").isEqualTo(22);
        assertThat(record211.get(0)).as("Record should start with 211").isEqualTo("211");
        assertThat(record211.get(1)).as("SSN is correct").isEqualTo(ssn);
        //assertThat(record211.get(2)).as("Payment ID is correct").isEqualTo(trans.getPaymentID());
        assertThat(record211.get(3)).as("OCR number is correct").isEqualTo(ocr);
        assertThat(record211.get(4)).as("Case ID is correct").isEqualTo(caseId);
        assertThat(record211.get(5)).as("Full payment is correct").isEqualTo("0");
        assertThat(record211.get(6)).as("Paid amount is correct").isEqualTo(amount);
        assertThat(record211.get(7)).as("Payment is correct").isEqualTo("A");
        assertThat(record211.get(8)).as("Debtor Row num is correct").isEqualTo(debtorRowNum);
        assertThat(record211.get(9)).as("Terms and conditions agreement flag is empty").isEqualTo("");
        //  assertThat(record211.get(10)).as("Original Transaction ID").isEqualTo(trans.getID());
        assertThat(record211.get(11)).as("Creation time stamp is correct").contains(formatDateTimeForFile);
        assertThat(record211.get(12)).as("Payment Provider is correct").isEqualTo(paymentProvider);
        assertThat(record211.get(13)).as("Merchant ID is empty").isEqualTo("");
        assertThat(record211.get(14)).as("Merchant ID is empty").isEqualTo("");
        assertThat(record211.get(15)).as("Response code is empty").isEqualTo("");
        assertThat(record211.get(16)).as("Login source is empty").isEqualTo("");
        assertThat(record211.get(17)).as("Login ID is empty").isEqualTo("");
        assertThat(record211.get(18)).as("Email type is empty").isEqualTo("");
        assertThat(record211.get(19)).as("Email address is empty").isEqualTo("");
        assertThat(record211.get(20)).as("Caller phone number is empty").isEqualTo("");

        //Assert 299 record
        CSVRecord record299 = records.get(2);
        System.out.println("SIZE OF RECORD:" + record211.size());
        assertThat(record299.size()).as("Field count is correct").isEqualTo(3);
        assertThat(record299.get(0)).as("Record should start with 299").isEqualTo("299");
        assertThat(record299.get(1)).as("Line count should be 3").isEqualTo("3");
    }

    @And("^I validate Free Text message export file content with params$")
    public void iValidateFreeTextMessageContent(DataTable table) throws IOException, SQLException, SftpException {

        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        String prodSystem = data.get(0).get("PROD_SYSTEM");
        String ssn = data.get(0).get("SSN");
        String nameSurname = data.get(0).get("NAME_SURNAME");
        String phoneHome = data.get(0).get("PHONE_HOME");
        String emailAddress = data.get(0).get("EMAIL_ADDRESS");
        String caseId = data.get(0).get("CASE_ID");
        String replyMethod = data.get(0).get("REPLY_METHOD");
        String messageText = data.get(0).get("MESSAGE_TEXT");
        String debtorRowNum = data.get(0).get("DEBTOR_ROW_NUMBER");
        String debtorWebMessage = data.get(0).get("DEBTOR_WEB_MESSAGE");
        String path = data.get(0).get("PATH");

        InputStream in = sftp.get(path + "/" + exportFileName);
        Reader targetReader = new InputStreamReader(in);
        CSVParser csvParser = new CSVParser(targetReader, CSVFormat.newFormat(';'));
        //caseId = getRandomizedString(caseId);
//        CaseDatabaseSteps caseDatabaseSteps = new CaseDatabaseSteps();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formatDateTime = now.format(formatter);
        DateTimeFormatter formatterForFile = DateTimeFormatter.ofPattern("yyyyMMdd");
        String formatDateTimeForFile = now.format(formatterForFile);
        List<CSVRecord> records = csvParser.getRecords();

        //Assert 200 record
        CSVRecord record200 = records.get(0);
        System.out.println("SIZE OF RECORD:" + record200.size());
        assertThat(record200.size()).as("Field count is correct").isEqualTo(5);
        assertThat(record200.get(0)).as("Record should start with 200").isEqualTo("200");
        assertThat(record200.get(1)).as("Date is correct").contains(formatDateTime);
        assertThat(record200.get(3)).as("Production system is correct").isEqualTo(prodSystem);

        //Assert 207 record
        CSVRecord record207 = records.get(1);
        //ssn = getRandomizedString(ssn);

        System.out.println("SIZE OF RECORD:" + record207.size());
        assertThat(record207.size()).as("Field count is correct").isEqualTo(21);
        assertThat(record207.get(0)).as("Record should start with 207").isEqualTo("207");
        assertThat(record207.get(1)).as("Debtor number is correct").isEqualTo(ssn);
        assertThat(record207.get(2)).as("Name1 is correct").isEqualTo(nameSurname);
        assertThat(record207.get(3)).as("Name2 is correct is empty").isEqualTo("");
        assertThat(record207.get(4)).as("Phone Home is correct is empty").isEqualTo(phoneHome);
        assertThat(record207.get(5)).as("Phone Work is correct is empty").isEqualTo("");
        assertThat(record207.get(6)).as("Mobile phone is correct is empty").isEqualTo("");
        assertThat(record207.get(7)).as("Email address is correct is empty").isEqualTo(emailAddress);
        assertThat(record207.get(8)).as("Case id is correct is correct").isEqualTo(caseId);
        assertThat(record207.get(9)).as("Reply method is empty").isEqualTo(replyMethod);
        assertThat(record207.get(10)).as("Text message is correct").isEqualTo(messageText);
        assertThat(record207.get(11)).as("Company ID part 1 (NL) is empty").contains("");
        assertThat(record207.get(12)).as("Company ID part 1 (NL) is empty").isEqualTo("");
        assertThat(record207.get(13)).as("Debtor’s row number is correct").isEqualTo(debtorRowNum);
        assertThat(record207.get(14)).as("Creation time stamp is correct").contains(formatDateTimeForFile);
        //assertThat(record207.get(15)).as("Debtor web message id is correct").isEqualTo(debtorWebMessage);
        assertThat(record207.get(16)).as("Related production system message id is empty").isEqualTo("");
        assertThat(record207.get(17)).as("Debtor birth date is empty").isEqualTo("");
        assertThat(record207.get(18)).as("Consent to use email is empty").isEqualTo("");
        assertThat(record207.get(19)).as("Consent to use phone number is empty").isEqualTo("");

        //Assert 299 record
        CSVRecord record299 = records.get(2);
        System.out.println("SIZE OF RECORD:" + record207.size());
        assertThat(record299.size()).as("Field count is correct").isEqualTo(3);
        assertThat(record299.get(0)).as("Record should start with 299").isEqualTo("299");
        assertThat(record299.get(1)).as("Line count should be 3").isEqualTo("3");
    }

    @And("^I validate Have Paid message export file content with params$")
    public void iValidateHavePaidMessageContent(DataTable table) throws IOException, SQLException, SftpException {

        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        String prodSystem = data.get(0).get("PROD_SYSTEM");
        String ssn = data.get(0).get("SSN");
        String nameSurname = data.get(0).get("NAME_SURNAME");
        String phoneHome = data.get(0).get("PHONE_HOME");
        String emailAddress = data.get(0).get("EMAIL_ADDRESS");
        String caseId = data.get(0).get("CASE_ID");
        String messageText = data.get(0).get("MESSAGE_TEXT");
        String paidCapital = data.get(0).get("PAID_CAPITAL");
        String paidTo = data.get(0).get("PAID_TO");
        String paidThrought = data.get(0).get("PAID_THROUGHT");
        String bankPlusNumber = data.get(0).get("BANK_PLUS_GIRO");
        String kidOcrNumber = data.get(0).get("KID_OCR_NUMBER");
        String debtorRowNum = data.get(0).get("DEBTOR_ROW_NUMBER");
        String debtorWebMessage = data.get(0).get("DEBTOR_WEB_MESSAGE");
        String replyMethod = data.get(0).get("REPLY_METHOD");
        String path = data.get(0).get("PATH");

        InputStream in = sftp.get(path + "/" + exportFileName);
        Reader targetReader = new InputStreamReader(in);
        CSVParser csvParser = new CSVParser(targetReader, CSVFormat.newFormat(';'));
//        caseId = getRandomizedString(caseId);
//        CaseDatabaseSteps caseDatabaseSteps = new CaseDatabaseSteps();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formatDateTime = now.format(formatter);
        DateTimeFormatter formatterForFile = DateTimeFormatter.ofPattern("yyyyMMdd");
        String formatDateTimeForFile = now.format(formatterForFile);
        List<CSVRecord> records = csvParser.getRecords();

        //Assert 200 record
        CSVRecord record200 = records.get(0);
        System.out.println("SIZE OF RECORD:" + record200.size());
        assertThat(record200.size()).as("Field count is correct").isEqualTo(5);
        assertThat(record200.get(0)).as("Record should start with 200").isEqualTo("200");
        assertThat(record200.get(1)).as("Date is correct").contains(formatDateTime);
        assertThat(record200.get(3)).as("Production system is correct").isEqualTo(prodSystem);

        //Assert 201 record
        CSVRecord record201 = records.get(1);
//        ssn = getRandomizedString(ssn);

        System.out.println("SIZE OF RECORD:" + record201.size());
        assertThat(record201.size()).as("Field count is correct").isEqualTo(25);
        assertThat(record201.get(0)).as("Record should start with 207").isEqualTo("201");
        assertThat(record201.get(1)).as("Debtor number is correct").isEqualTo(ssn);
        assertThat(record201.get(2)).as("Name1 is correct").isEqualTo(nameSurname);
        assertThat(record201.get(3)).as("Name2 is correct is empty").isEqualTo("");
        assertThat(record201.get(4)).as("Phone Home is correct is empty").isEqualTo(phoneHome);
        assertThat(record201.get(5)).as("Phone Work is correct is empty").isEqualTo("");
        assertThat(record201.get(6)).as("Mobile phone is correct is empty").isEqualTo("");
        assertThat(record201.get(7)).as("Email address is correct is empty").isEqualTo(emailAddress);
        assertThat(record201.get(8)).as("Case id is correct is correct").isEqualTo(caseId);
        assertThat(record201.get(9)).as("Paid capital is correct").isEqualTo(paidCapital);
        assertThat(record201.get(10)).as("Paid to is correct").isEqualTo(paidTo);
        assertThat(record201.get(11)).as("Paid throught is correct").contains(paidThrought);
        assertThat(record201.get(12)).as("Paid to Bank/Plusgiro number is correct").isEqualTo(bankPlusNumber);
        assertThat(record201.get(13)).as("KID or OCR number is correct").isEqualTo(kidOcrNumber);
        assertThat(record201.get(14)).as("Text message is correct").isEqualTo(messageText);
        assertThat(record201.get(15)).as("Paid Date is correct").contains(formatDateTimeForFile);
        assertThat(record201.get(16)).as("Company ID part 1 (NL) is empty").contains("");
        assertThat(record201.get(17)).as("Company ID part 1 (NL) is empty").isEqualTo("");
        assertThat(record201.get(18)).as("Debtor’s row number is correct").isEqualTo(debtorRowNum);
        assertThat(record201.get(19)).as("Creation time stamp is correct").contains(formatDateTimeForFile);
        assertThat(record201.get(20)).as("Reply method is empty").isEqualTo(replyMethod);
        //assertThat(record201.get(21)).as("Debtor web message id is correct").isEqualTo(debtorWebMessage);
        assertThat(record201.get(22)).as("Related production system message id is empty").isEqualTo("");
        assertThat(record201.get(23)).as("Debtor birth date is empty").isEqualTo("");

        //Assert 299 record
        CSVRecord record299 = records.get(2);
        System.out.println("SIZE OF RECORD:" + record201.size());
        assertThat(record299.size()).as("Field count is correct").isEqualTo(3);
        assertThat(record299.get(0)).as("Record should start with 299").isEqualTo("299");
        assertThat(record299.get(1)).as("Line count should be 3").isEqualTo("3");
    }

    private String getRandomizedString(String randomized) {
        if (System.getProperty("random") == null) {
            System.setProperty("random", "5");
        }
        if (randomized.equals("")) {
            return randomized;
        }
        return randomized + System.getProperty("random");
    }

}

