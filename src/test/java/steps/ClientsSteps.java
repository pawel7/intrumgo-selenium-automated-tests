package steps;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import lombok.ToString;
import pageObjects.ChromeDownloadPage;
import pageObjects.MainPage;
import utils.Screenshots;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ClientsSteps extends BaseSteps {

    ChromeDownloadPage chromeDownloadPage;
    MainPage mainPage;


    @Before
    public void before(Scenario scenario) throws Exception {
        Screenshots screen = new Screenshots();
        scenarioName = screen.getNameOfScenario(scenario);
        featureName = screen.getNameOfFeature(scenario);
        updateExtentTest(featureName, scenarioName);
        mainPage = new MainPage(driver, wait);
    }

    @And("^User sees (.*) clients with status (.*)$")
    public void userSeesClientsWithStatus(int count, String status) {
        mainPage.clientsList().waitForElementPresents();
//        assertThat(!mainPage.clientRow("1").isPresents()).as("Notifications are not displayed").isTrue();
        mainPage.assertThatEquals(String.valueOf(mainPage.clientsList().getItemCount()), String.valueOf(count), "There are correct amount of clients");
        if (status.equals("PENDING"))
            mainPage.assertThatEquals(String.valueOf(mainPage.clientsStatusesListPending().getItemCount()), String.valueOf(count), "There are correct amount of clients with status pending");
        else if (status.equals("REJECTED"))
            mainPage.assertThatEquals(String.valueOf(mainPage.clientsStatusesListRejected().getItemCount()), String.valueOf(count), "There are correct amount of clients with status pending");
        else if (status.equals("APPROVED"))
            mainPage.assertThatEquals(String.valueOf(mainPage.clientsStatusesListApproved().getItemCount()), String.valueOf(count), "There are correct amount of clients with status pending");
        else System.out.println("User is not able to see any case");
    }

    @And("^User sees (.*) cases with status (.*)$")
    public void userSeesCasessWithStatus(int count, String status) {
        mainPage.casesList().waitForElementPresents();
//        assertThat(!mainPage.clientRow("1").isPresents()).as("Notifications are not displayed").isTrue();
        mainPage.assertThatEquals(String.valueOf(mainPage.casesList().getItemCount()), String.valueOf(count), "There are correct amount of clients");
        if (status.equals("PENDING"))
            mainPage.assertThatEquals(String.valueOf(mainPage.casesStatusesListPending().getItemCount()), String.valueOf(count), "There are correct amount of clients with status pending");
        else if (status.equals("REJECTED"))
            mainPage.assertThatEquals(String.valueOf(mainPage.casesStatusesListRejected().getItemCount()), String.valueOf(count), "There are correct amount of clients with status pending");
        else if (status.equals("APPROVED"))
            mainPage.assertThatEquals(String.valueOf(mainPage.casesStatusesListApproved().getItemCount()), String.valueOf(count), "There are correct amount of clients with status pending");
        else System.out.println("User is not able to see any case");
    }

    @And("^User sees Intrum icon$")
    public void userSeesIntrumIcon() {
        assertThat(mainPage.intrumLogo().isDisplayed())
                .as("Intrum header icon is displayed").isTrue();
    }

    @And("^User clicks Pending clients$")
    public void userClicksPendingClients() {
        mainPage.clientsPending().click();
    }

    @And("^User clicks Rejected clients$")
    public void userClicksRejectedClients() {
        mainPage.clientsRejected().click();
    }

    @And("^User clicks Approved clients$")
    public void userClicksApprovedClients() {
        mainPage.clientsApproved().click();
    }

    @And("^User clicks Pending cases$")
    public void userClicksPendingCases() {
        mainPage.casesPending().click();

//        try {
//            String[] cmd = { "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe", "-incognito", "-o" };
//            Runtime.getRuntime().exec(cmd);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    @And("^User clicks Rejected cases$")
    public void userClicksRejectedCases() {
        mainPage.casesRejected().click();
    }

    @And("^User clicks Approved cases$")
    public void userClicksApprovedCases() {
        mainPage.casesApproved().click();
    }


    @And("^All fields are visible on simple login page$")
    public void allFieldsAreVisibleOnSimpleLoginPage() {
        mainPage.checkIfAllClientsTitleElementsAreVisible();
    }

    @Then("^User sees following table titles for clients section$")
    public void userIsAbleToSeeClientsTitlesSection(DataTable labels) {
        List<Map<String, String>> data = labels.asMaps(String.class, String.class);
        String column1 = data.get(0).get("COLUMN1");
        String column2 = data.get(0).get("COLUMN2");
        String column3 = data.get(0).get("COLUMN3");
        String column4 = data.get(0).get("COLUMN4");
        String column5 = data.get(0).get("COLUMN5");
        String column6 = data.get(0).get("COLUMN6");
        String column7 = data.get(0).get("COLUMN7");
        String column8 = data.get(0).get("COLUMN8");

        assertThat(mainPage.clientsTitleColumnX("1").getValue()).isEqualTo(column1);
        assertThat(mainPage.clientsTitleColumnX("2").getValue()).isEqualTo(column2);
        assertThat(mainPage.clientsTitleColumnX("3").getValue()).isEqualTo(column3);
        assertThat(mainPage.clientsTitleColumnX("4").getValue()).isEqualTo(column4);
        assertThat(mainPage.clientsTitleColumnX("5").getValue()).isEqualTo(column5);
        assertThat(mainPage.clientsTitleColumnX("6").getValue()).isEqualTo(column6);
        assertThat(mainPage.clientsTitleColumnX("7").getValue()).isEqualTo(column7);
        assertThat(mainPage.clientsTitleColumnX("8").getValue()).isEqualTo(column8);
    }

    @Then("^User sees following table titles for cases section$")
    public void userIsAbleToSeeCasesTitlesSection(DataTable labels) {
        List<Map<String, String>> data = labels.asMaps(String.class, String.class);
        String column1 = data.get(0).get("COLUMN1");
        String column2 = data.get(0).get("COLUMN2");
        String column3 = data.get(0).get("COLUMN3");
        String column4 = data.get(0).get("COLUMN4");
        String column5 = data.get(0).get("COLUMN5");
        String column6 = data.get(0).get("COLUMN6");
        String column7 = data.get(0).get("COLUMN7");
        String column8 = data.get(0).get("COLUMN8");

        assertThat(mainPage.casesTitleColumnX("1").getValue()).isEqualTo(column1);
        assertThat(mainPage.casesTitleColumnX("2").getValue()).isEqualTo(column2);
        assertThat(mainPage.casesTitleColumnX("3").getValue()).isEqualTo(column3);
        assertThat(mainPage.casesTitleColumnX("4").getValue()).isEqualTo(column4);
        assertThat(mainPage.casesTitleColumnX("5").getValue()).isEqualTo(column5);
        assertThat(mainPage.casesTitleColumnX("6").getValue()).isEqualTo(column6);
        assertThat(mainPage.casesTitleColumnX("7").getValue()).isEqualTo(column7);
        assertThat(mainPage.casesTitleColumnX("8").getValue()).isEqualTo(column8);
    }


    @Then("^User sees Clients filters$")
    public void userIsAbleToSeeClientsStatusesFilter(DataTable labels) {
        List<Map<String, String>> data = labels.asMaps(String.class, String.class);
        String clients = data.get(0).get("CLIENTS");
        String pending = data.get(0).get("PENDING");
        String rejected = data.get(0).get("REJECTED");
        String approved = data.get(0).get("APPROVED");

        assertThat(mainPage.clientsStatusesFilterTitle().getValue()).isEqualTo(clients);
        assertThat(mainPage.clientsPending().getValue()).isEqualTo(pending);
        assertThat(mainPage.clientsRejected().getValue()).isEqualTo(rejected);
        assertThat(mainPage.clientsApproved().getValue()).isEqualTo(approved);
    }

    @Then("^User sees Cases filters$")
    public void userIsAbleToSeeCasesStatusesFilter(DataTable labels) {
        List<Map<String, String>> data = labels.asMaps(String.class, String.class);
        String cases = data.get(0).get("CASES");
        String pending = data.get(0).get("PENDING");
        String rejected = data.get(0).get("REJECTED");
        String approved = data.get(0).get("APPROVED");

        assertThat(mainPage.casesStatusesFilterTitle().getValue()).isEqualTo(cases);
        assertThat(mainPage.casesPending().getValue()).isEqualTo(pending);
        assertThat(mainPage.casesRejected().getValue()).isEqualTo(rejected);
        assertThat(mainPage.casesApproved().getValue()).isEqualTo(approved);
    }

    @And("^User clicks next page$")
    public void userClicksNextPage() {
        mainPage.nextPage().click();
        try {
            Thread.sleep( 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @And("^User clicks prev page$")
    public void userClicksPrevPage() {
        mainPage.prevPage().click();
        try {
            Thread.sleep( 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Then("^User sees following case in row: (.*)$")
    public void userIsAbleToSeeCasesTitlesSection(Integer rownNumberInt, DataTable labels) {
        List<Map<String, String>> data = labels.asMaps(String.class, String.class);
        String column1 = data.get(0).get("COLUMN1");
        String column2 = data.get(0).get("COLUMN2");
        String column3 = data.get(0).get("COLUMN3");
        String column4 = data.get(0).get("COLUMN4");
        String column5 = data.get(0).get("COLUMN5");
        String column6 = data.get(0).get("COLUMN6");
        String column8 = data.get(0).get("COLUMN8");

        rownNumberInt = rownNumberInt + 1;
        String rownNumber = rownNumberInt.toString();

        assertThat(mainPage.casesTitleRowsXColumnY(rownNumber,"1").getValue()).isEqualTo(column1);
        assertThat(mainPage.casesTitleRowsXColumnY(rownNumber,"2").getValue()).isEqualTo(column2);
        assertThat(mainPage.casesTitleRowsXColumnY(rownNumber,"3").getValue()).isEqualTo(column3);
        assertThat(mainPage.casesTitleRowsXColumnY(rownNumber,"4").getValue()).isEqualTo(column4);
        assertThat(mainPage.casesTitleRowsXColumnY(rownNumber,"5").getValue()).isEqualTo(column5);
        assertThat(mainPage.casesTitleRowsXColumnY(rownNumber,"6").getValue()).isEqualTo(column6);
        assertThat(mainPage.casesTitleRowsXColumnY(rownNumber,"8").getValue()).isEqualTo(column8);
    }

    @Then("^User sees following client in row: (.*)$")
    public void userIsAbleToSeeClientsTitlesSection(Integer rownNumberInt, DataTable labels) {
        List<Map<String, String>> data = labels.asMaps(String.class, String.class);
        String column1 = data.get(0).get("COLUMN1");
        String column2 = data.get(0).get("COLUMN2");
        String column3 = data.get(0).get("COLUMN3");
        String column4 = data.get(0).get("COLUMN4");
        String column5 = data.get(0).get("COLUMN5");
        String column6 = data.get(0).get("COLUMN6");
        String column8 = data.get(0).get("COLUMN8");

        rownNumberInt = rownNumberInt + 1;
        String rownNumber = rownNumberInt.toString();

        assertThat(mainPage.clientsTitleRowsXColumnY(rownNumber,"1").getValue()).isEqualTo(column1);
        assertThat(mainPage.clientsTitleRowsXColumnY(rownNumber,"2").getValue()).isEqualTo(column2);
        assertThat(mainPage.clientsTitleRowsXColumnY(rownNumber,"3").getValue()).isEqualTo(column3);
        assertThat(mainPage.clientsTitleRowsXColumnY(rownNumber,"4").getValue()).isEqualTo(column4);
        assertThat(mainPage.clientsTitleRowsXColumnY(rownNumber,"5").getValue()).isEqualTo(column5);
        assertThat(mainPage.clientsTitleRowsXColumnY(rownNumber,"6").getValue()).isEqualTo(column6);
        assertThat(mainPage.clientsTitleRowsXColumnY(rownNumber,"8").getValue()).isEqualTo(column8);
    }

}


