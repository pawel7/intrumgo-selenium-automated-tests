package steps.databaseSteps;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cucumber.api.java.en.Given;
import org.apache.commons.io.FileUtils;
import steps.BaseSteps;
import utils.ConnectionManager;
import utils.InternalClasses.Transaction;
import utils.TestProperties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;

public class CaseDatabaseSteps extends BaseSteps {

    protected static Properties properties;

    static {
        properties = TestProperties.getInstance();
    }

    private Connection con;
    private Statement stmt;
    private ResultSet rs;

    public void connectToCasesDatabase() throws SQLException {
        System.out.println("Connection created");
        con = ConnectionManager.getConnection(
                properties.getProperty("oracle.url"),
                properties.getProperty("oracle.cases.username"),
                properties.getProperty("oracle.cases.password"),
                properties.getProperty("oracle.driver.name")
        );
        System.out.println("Connected to:");
        System.out.println(properties.getProperty("oracle.url"));
    }


    @Given("^I insert test client cases in database using CSV$")
    public void iInsertTestClientCasesInDatabaseUsingCSV() throws SQLException, FileNotFoundException {
        connectToCasesDatabase();
        Statement stmt = con.createStatement();
        String INSERT_RECORD = "INSERT INTO " +
                "CLIENT_CASE(" +
                "CASE_REFERENCE," +
                "PRODUCTION_SYSTEM_ID," +
                "CASE_NUMBER," +
                "AMOUNT_DUE," +
                "AMOUNT_DUE_CURRENCY," +
                "CLIENT_ID," +
                "CLIENT_NAME," +
                "STATUS)" +
                "values(?,?,?,?,?,?,?,?)";

        PreparedStatement pstmt = con.prepareStatement(INSERT_RECORD);
        Scanner scanner = new Scanner(new File("testData/clientCases.csv"));
        Scanner dataScanner = null;
        int index = 0;
        while (scanner.hasNextLine()) {
            dataScanner = new Scanner(scanner.nextLine());
            dataScanner.useDelimiter(";");
            System.out.print("INSERT INTO " +
                    "CLIENT_CASE(" +
                    "CASE_REFERENCE," +
                    "PRODUCTION_SYSTEM_ID," +
                    "CASE_NUMBER," +
                    "AMOUNT_TOTAL," +
                    "AMOUNT_TOTAL_CURRENCY," +
                    "AMOUNT_DUE," +
                    "AMOUNT_DUE_CURRENCY," +
                    "CLIENT_ID," +
                    "CLIENT_NAME," +
                    "STATUS" +
                    ")values(");
            while (dataScanner.hasNext()) {
                String data = dataScanner.next();
                if (index == 0)//case reference
                {
                    pstmt.setString(1, data + System.getProperty("random"));
                    System.out.print(data + System.getProperty("random") + ",");
                } else if (index == 1) {
                    pstmt.setInt(2, Integer.parseInt(data));
                    System.out.print(Integer.parseInt(data) + ",");
                } else if (index == 2) {
                    pstmt.setString(3, data + System.getProperty("random"));
                    System.out.print(data + System.getProperty("random") + ",");
                } else if (index == 3) {
                } else if (index == 4) {

                } else if (index == 5) {
                    pstmt.setDouble(4, Double.parseDouble(data));
                    System.out.print(Double.parseDouble(data) + ",");
                } else if (index == 6) {
                    pstmt.setString(5, data);
                    System.out.print(data + ",");
                } else if (index == 7) {
                    pstmt.setString(6, data);
                    System.out.print(data + ",");
                } else if (index == 8) {
                    pstmt.setString(7, data);
                    System.out.print(data + ",");
                } else if (index == 9) {
                    pstmt.setString(8, data);
                    System.out.print(data);
                } else
                    System.out.println("invalid data::" + data);
                index++;
            }
            System.out.print(")");
            System.out.println("");
            index = 0;
            pstmt.addBatch();
        }
        scanner.close();
        System.out.println(pstmt.getParameterMetaData());
        pstmt.executeBatch();
        closeConnectionToCasesDatabase();
    }

    @Given("^I insert test obligations in database using CSV$")
    public void iInsertTestObligationsInDatabaseUsingCSV() throws SQLException, FileNotFoundException {
        connectToCasesDatabase();
        Statement stmt = con.createStatement();
        String INSERT_RECORD = "INSERT INTO " +
                "OBLIGATION(" +
                "OBLIGATION_REFERENCE," +
                "CASE_REFERENCE," +
                "PRODUCTION_SYSTEM_ID," +
                "CASE_NUMBER," +
                "DEBTOR_ROW_NUM," +
                "DEBTOR_NUMBER," +
                "OCR_NUMBER," +
                "CAN_APPLY_FOR_PAYMENT_PLAN," +
                "OBLIGATION_STATUS," +
                "VISIBLE_TO_CUSTOMER)" +
                "values(?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement pstmt = con.prepareStatement(INSERT_RECORD);
        Scanner scanner = new Scanner(new File("testData/obligation.csv"));
        Scanner dataScanner = null;
        int index = 0;
        while (scanner.hasNextLine()) {
            dataScanner = new Scanner(scanner.nextLine());
            dataScanner.useDelimiter(";");
            String caseRef = null;
            while (dataScanner.hasNext()) {
                String data = dataScanner.next();
                if (index == 0) {//case ref
                    caseRef = data + System.getProperty("random");
                    pstmt.setString(2, caseRef);
                }

                if (index == 1)//prod system
                    pstmt.setString(3, data);
                if (index == 2)//case number
                    pstmt.setString(4, data + System.getProperty("random"));
                if (index == 3) {//debtor row number{
                    pstmt.setString(5, data + System.getProperty("random"));
                    pstmt.setString(1, caseRef + "::DE" + data + System.getProperty("random"));
                }
                if (index == 4) {//debtor number
                    System.out.println("DATA IS :" + data);
                    if (data == null || data.equals("")) {
                        pstmt.setNull(6, Types.VARCHAR);
                    } else {
                        pstmt.setString(6, data + System.getProperty("random"));
                    }

                }
                if (index == 5) {//ocr_number
                    pstmt.setString(7, data);
                }
                if (index == 6) {//can_apply_for_payment_plan
                    pstmt.setString(8, data);
                }
                if (index == 7) {//obligation_Status
                    pstmt.setString(9, data);
                }
                if (index == 8) {//visible_to_customer
                    pstmt.setString(10, data);
                }
                index++;
            }
            index = 0;
            pstmt.addBatch();
        }
        scanner.close();
        pstmt.executeBatch();
        closeConnectionToCasesDatabase();
    }


    public Transaction getAllTransactionsForCase(String caseRef)
            throws SQLException {
//        String caseReference = getRandomizedString(caseRef);
        String caseReference = caseRef;
        connectToCasesDatabase();
        String selectSQL = "select " +
                "PRODUCTION_SYSTEM_ID," +
                "CASE_REFERENCE,AMOUNT," +
                "AMOUNT_CURRENCY,PAYMENT_ID," +
                "STATUS," +
                "TRANSACTION_DATE," +
                "ID" +
                " from transactions" +
                " where " +
                "CASE_REFERENCE = " + caseReference + " and rownum=1 order by TRANSACTION_DATE desc";
        PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
        ResultSet rs = preparedStatement.executeQuery(selectSQL);
        Transaction trans = null;
        while (rs.next()) {
            trans = new Transaction(
                    rs.getString("PRODUCTION_SYSTEM_ID"),
                    rs.getString("CASE_REFERENCE"),
                    rs.getString("AMOUNT"),
                    rs.getString("AMOUNT_CURRENCY"),
                    rs.getString("PAYMENT_ID"),
                    rs.getString("STATUS"),
                    rs.getString("TRANSACTION_DATE"),
                    rs.getString("ID")
            );
        }
        closeConnectionToCasesDatabase();
        return trans;
    }

    @Given("^I verify all transaction for case (.*) values are (.*)/(.*)/(.*)/(.*)")
    public void verifyAllTransactionValues(String caseRef, String amount, String currency, String prodSys, String status) throws SQLException {
        Transaction trans = getAllTransactionsForCase(caseRef);
        assertThat(trans).as("Transaction is saved to DB").isNotEqualTo(null);
        assertThat(trans.getAmount()).as("Amount is correct").isEqualTo(amount);
        assertThat(trans.getCurrency()).as("Currency is correct").isEqualTo(currency);
        assertThat(trans.getProductionSystemId()).as("ProdSys is correct").isEqualTo(prodSys);
        assertThat(trans.getStatus()).as("Status is correct").isEqualTo(status);

    }

    @Given("^I verify export data corresponds to transaction case (.*) data")
    public void verifyAllTransactionsForExport(String caseRef) throws SQLException {
        Transaction trans = getAllTransactionsForCase(caseRef);
        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create();
        System.out.println(gson.toJson(trans));
    }

    @Given("^I verify all transaction for case (.*) values is none")
    public void verifyAllTransactionValuesIsNone(String caseRef) throws SQLException {
        Transaction trans = getAllTransactionsForCase(caseRef);
        assertThat(trans).as("Transaction is saved to DB").isEqualTo(null);
    }

    public void closeConnectionToCasesDatabase() throws SQLException {
        System.out.println("Connection closed");
        con.close();
    }

    private String getRandomizedString(String randomized) {
        if (System.getProperty("random") == null) {
            System.setProperty("random", "5");
        }
        return "'" + randomized + System.getProperty("random") + "'";
    }

    @Given("^I delete empty JSONS$")
    public void iDeleteEmptyJSONS() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        deleteEmptyJSONS();
    }

    public void deleteEmptyJSONS() {

        System.out.println("DELETING THE CASES");

        File dir = new File("target/cucumber-parallel");
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.length() == 0) {
                    System.out.println("EMPTY FILE FOUND");
                    System.out.println(child.getName());
                    child.delete();
                }
            }
        } else {
            // Handle the case where dir is not really a directory.
            // Checking dir.isDirectory() above would not be sufficient
            // to avoid race conditions with another process that deletes
            // directories.
        }
    }

    @Given("^i insert financial data in database using CSV$")
    public void iInsertFinancialDataInDatabaseUsingCSV() throws Throwable {
        connectToCasesDatabase();
        Statement stmt = con.createStatement();
        String INSERT_RECORD = "INSERT INTO " +
                "CLIENT_CASE_FINANCIAL_SUMMARY(" +
                "CASE_REFERENCE," +
                "ORIGINAL_CAPITAL," +
                "ORIGINAL_CAPITAL_CUR," +
                "REMAINING_CAPITAL," +
                "REMAINING_CAPITAL_CUR," +
                "REMAINING_CLIENT_COSTS," +
                "REMAINING_CLIENT_COSTS_CUR," +
                "REMAINING_CLIENT_FEE," +
                "REMAINING_CLIENT_FEE_CUR," +
                "REMAINING_CLIENT_OUTLAY," +
                "REMAINING_CLIENT_OUTLAY_CUR," +
                "REMAINING_DEBTOR_FEE," +
                "REMAINING_DEBTOR_FEE_CUR," +
                "REMAINING_DEBTOR_OUTLAY," +
                "REMAINING_DEBTOR_OUTLAY_CUR," +
                "REMAINING_INTEREST," +
                "REMAINING_INTEREST_CUR," +
                "INTEREST_CALCULATION_DATE," +
                "AMOUNT_DUE," +
                "AMOUNT_DUE_CUR," +
                "CASE_CONCERN)" +
                "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement pstmt = con.prepareStatement(INSERT_RECORD);
        Scanner scanner = new Scanner(new File("testData/caseFinancialSummary.csv"));
        Scanner dataScanner = null;
        int index = 0;
        while (scanner.hasNextLine()) {
            dataScanner = new Scanner(scanner.nextLine());
            dataScanner.useDelimiter(";");
            String caseRef = null;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy.mm.dd");
            while (dataScanner.hasNext()) {
                String data = dataScanner.next();
                if (index == 0) {//CASE_REFERENCE
                    caseRef = data + System.getProperty("random");
                    pstmt.setString(1, caseRef);
                }
                if (index == 1)//ORIGINAL_PRINCIPAL
                    pstmt.setDouble(2, Double.parseDouble(data));
                if (index == 2)//ORIGINAL_PRINCIPAL_CUR
                    pstmt.setString(3, data);
                if (index == 3)//REMAINING_PRINCIPAL
                    pstmt.setDouble(4, Double.parseDouble(data));
                if (index == 4)//REMAINING_PRINCIPAL_CUR
                    pstmt.setString(5, data);
                if (index == 5)//REMAINING_CLIENT_COSTS
                    pstmt.setDouble(6, Double.parseDouble(data));
                if (index == 6)//REMAINING_CLIENT_COSTS_CUR
                    pstmt.setString(7, data);
                if (index == 7)//REMAINING_CLIENT_FEE
                    pstmt.setDouble(8, Double.parseDouble(data));
                if (index == 8)//REMAINING_CLIENT_FEE_CUR
                    pstmt.setString(9, data);
                if (index == 9)//REMAINING_CLIENT_OUTLAY
                    pstmt.setDouble(10, Double.parseDouble(data));
                if (index == 10)//REMAINING_CLIENT_OUTLAY_CUR
                    pstmt.setString(11, data);
                if (index == 11)//REMAINING_CUSTOMER_FEE
                    pstmt.setDouble(12, Double.parseDouble(data));
                if (index == 12)//REMAINING_CUSTOMER_FEE_CUR
                    pstmt.setString(13, data);
                if (index == 13)//REMAINING_CUSTOMER_OUTLAY
                    pstmt.setDouble(14, Double.parseDouble(data));
                if (index == 14)//REMAINING_CUSTOMER_OUTLAY_CUR
                    pstmt.setString(15, data);
                if (index == 15)//REMAINING_INTEREST
                    pstmt.setDouble(16, Double.parseDouble(data));
                if (index == 16)//REMAINING_INTEREST_CUR
                    pstmt.setString(17, data);
                if (index == 17)//INTEREST_CALCULATION_DATE
                    pstmt.setDate(18, new java.sql.Date(formatter.parse(data).getTime()));
                if (index == 18)//AMOUNT_DUE
                    pstmt.setDouble(19, Double.parseDouble(data));
                if (index == 19)//AMOUNT_DUE_CUR
                    pstmt.setString(20, data);
                if (index == 20)//CASE_CONCERN
                    pstmt.setString(21, data);
                index++;
            }
            index = 0;
            pstmt.addBatch();
        }
        scanner.close();
        pstmt.executeBatch();
        closeConnectionToCasesDatabase();
    }

    @Given("^I insert static cases and other items$")
    public void iInsertStaticCasesAndOtherItems() throws Throwable {
        connectToCasesDatabase();
        try {

            File f = new File("testData/staticData.csv");
            stmt = con.createStatement();
            List<String> lines = FileUtils.readLines(f, "UTF-8");

            for (String line : lines) {
                System.out.println(line);
                stmt.addBatch(line);
            }
            stmt.executeBatch();
            con.commit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        closeConnectionToCasesDatabase();


    }

    @Given("^I delete all transaction for case (.*)$")
    public void deleteAllTransactionsForCase(String caseRef)
            throws SQLException {

        connectToCasesDatabase();
        String selectSQL = "delete TRANSACTIONS where CASE_REFERENCE = '" + caseRef + "'";
        PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
        ResultSet rs = preparedStatement.executeQuery(selectSQL);
        closeConnectionToCasesDatabase();

    }
}