package steps.databaseSteps;

import cucumber.api.java.en.Given;
import steps.BaseSteps;
import utils.ConnectionManager;
import utils.InternalClasses.Payment;
import utils.TestProperties;

import java.sql.*;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public class PaymentDatabaseSteps extends BaseSteps {

    protected static Properties properties;

    static {
        properties = TestProperties.getInstance();
    }

    CaseDatabaseSteps caseDatabaseSteps = new CaseDatabaseSteps();
    private Connection con;
    private Statement stmt;
    private ResultSet rs;

    public void connectToPaymentDatabase() throws SQLException {
        System.out.println("Connection created");
        con = ConnectionManager.getConnection(
                properties.getProperty("oracle.url"),
                properties.getProperty("oracle.payment.username"),
                properties.getProperty("oracle.payment.password"),
                properties.getProperty("oracle.driver.name")
        );
        System.out.println("Connected to:");
        System.out.println(properties.getProperty("oracle.url"));
    }

    @Given("^I verify payment for particular transaction for case (.*) values are (.*)/(.*)/(.*)/(.*)/(.*)")
    public void iVerifyPaymentForParticularTransactionForCaseCA(
            String caseRef,
            String amount,
            String currency,
            String provider,
            String status,
            String merchant)
            throws SQLException {

        Payment payment = getPayment(caseRef);
        assertThat(payment).as("Is not null").isNotNull();
        assertThat(payment.getAmount()).as("Amount is correct").isEqualTo(amount);
        assertThat(payment.getCurrency()).as("currency is correct").isEqualTo(currency);
        assertThat(payment.getProvider()).as("provider is correct").isEqualTo(provider);
        assertThat(payment.getStatus()).as("status is correct").isEqualTo(status);
        assertThat(payment.getMerchant()).as("merchant is correct").isEqualTo(merchant);

    }

    public Payment getPayment(String caseRef) throws SQLException {
        return getAllPaymentsForTransaction(caseDatabaseSteps.getAllTransactionsForCase(caseRef).getPaymentID());
    }

    public Payment getPaymentFromId(String paymentId) throws SQLException {
        return getAllPaymentsForTransaction(paymentId);
    }


    public void closeConnectionToCasesDatabase() throws SQLException {
        System.out.println("Connection closed");
        con.close();
    }

    private String getRandomizedString(String randomized) {
        if (System.getProperty("random") == null) {
            System.setProperty("random", "5");
        }
        return "'" + randomized + System.getProperty("random") + "'";
    }

    public Payment getAllPaymentsForTransaction(String paymentId)
            throws SQLException {
        connectToPaymentDatabase();

        String selectSQL = "select amount,currency,provider,status,merchant from payment where id = '" + paymentId + "'";
        PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
        ResultSet rs = preparedStatement.executeQuery(selectSQL);
        Payment payment = null;
        while (rs.next()) {
            payment = new Payment(
                    rs.getString("amount"),
                    rs.getString("currency"),
                    rs.getString("provider"),
                    rs.getString("status"),
                    rs.getString("merchant")
            );
        }
        closeConnectionToCasesDatabase();
        return payment;
    }

}
