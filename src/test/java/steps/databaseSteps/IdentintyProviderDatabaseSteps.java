package steps.databaseSteps;

import cucumber.api.java.en.Given;
import org.mindrot.jbcrypt.BCrypt;
import steps.BaseSteps;
import utils.ConnectionManager;
import utils.TestProperties;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;
import java.util.UUID;

public class IdentintyProviderDatabaseSteps extends BaseSteps {

    protected static Properties properties;

    static {
        properties = TestProperties.getInstance();
    }

    private Connection con = null;
    private Statement stmt = null;
    private ResultSet rs = null;

    public void connectToIPDatabase() {
        System.out.println("Connection created");
        con = ConnectionManager.getConnection(
                properties.getProperty("oracle.url"),
                properties.getProperty("oracle.ip.username"),
                properties.getProperty("oracle.ip.password"),
                properties.getProperty("oracle.driver.name")
        );
        System.out.println("Connected to:");
        System.out.println(properties.getProperty("oracle.url"));

    }


    @Given("^I insert test users in database using CSV$")
    public void iInsertUsersUsingCSVFile() throws SQLException, FileNotFoundException {
        connectToIPDatabase();
        String INSERT_RECORD = "INSERT INTO " +
                "CUSTOMER_LOGIN(" +
                "CUSTOMER_LOGIN_ID," +
                "USERNAME," +
                "PASSWORD," +
                "PRODUCTION_SYSTEM_ID," +
                "COUNTRY_CODE," +
                "DEBTOR_CASE_REFERENCE) " +
                "values(?,?,?,?,?,?)";

        PreparedStatement pstmt = con.prepareStatement(INSERT_RECORD);
        Scanner scanner = new Scanner(new File("testData/users.csv"));
        Scanner dataScanner = null;
        int index = 0;
        while (scanner.hasNextLine()) {
            dataScanner = new Scanner(scanner.nextLine());
            dataScanner.useDelimiter(";");
            String debtorRefID = null;
            while (dataScanner.hasNext()) {
                String data = dataScanner.next();

                pstmt.setString(1, generateUUID().toString());
                if (index == 0)//username
                    pstmt.setString(2, data + System.getProperty("random"));
                else if (index == 1)//password
                    pstmt.setString(3, generateHashFromPassword(data));
                else if (index == 2)//prodsysID
                    pstmt.setString(4, data);//index = csv line position
                else if (index == 3)//countrycode
                    pstmt.setString(5, data); //parameterIndex  = column
                else if (index == 4)
                    debtorRefID = data + System.getProperty("random");
                else if (index == 5) {
                    debtorRefID = debtorRefID + "::" + data + System.getProperty("random");
                    pstmt.setString(6, debtorRefID);
                } else
                    System.out.println("invalid data::" + data);
                index++;
            }
            index = 0;
            pstmt.addBatch();
        }
        scanner.close();
        pstmt.executeBatch();
        closeConnectionToIPDatabase();
    }

    public void closeConnectionToIPDatabase() throws SQLException {
        System.out.println("Connection closed");
        con.close();
    }

    private UUID generateUUID() {
        UUID idOne = UUID.randomUUID();
        return idOne;
    }

    private String generateHashFromPassword(String password) {
        String hash = BCrypt.hashpw(password, BCrypt.gensalt(10));
        return hash;
    }

}