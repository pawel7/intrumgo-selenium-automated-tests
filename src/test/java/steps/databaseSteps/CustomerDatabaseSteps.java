package steps.databaseSteps;

import cucumber.api.java.en.Given;
import steps.BaseSteps;
import utils.ConnectionManager;
import utils.TestProperties;

import java.sql.*;
import java.util.Properties;

public class CustomerDatabaseSteps extends BaseSteps {

    protected static Properties properties;

    static {
        properties = TestProperties.getInstance();
    }

    CaseDatabaseSteps caseDatabaseSteps = new CaseDatabaseSteps();
    private Connection con;
    private Statement stmt;
    private ResultSet rs;

    public void connectToCustomerDatabase() throws SQLException {
        System.out.println("Connection created");
        con = ConnectionManager.getConnection(
                properties.getProperty("oracle.url"),
                properties.getProperty("oracle.profile.username"),
                properties.getProperty("oracle.profile.password"),
                properties.getProperty("oracle.driver.name")
        );
        System.out.println("Connected to:");
        System.out.println(properties.getProperty("oracle.url"));
    }

    @Given("^I add username (.*) and password (.*) for particular user (.*)")
    public void addUsernameAndPasswordForUser(String username, String password, String ssn) throws SQLException {
        connectToCustomerDatabase();

        String updateSQL = "UPDATE CUSTOMER SET FIRST_NAME = ?,LAST_NAME = ? where ssn = ?";
        PreparedStatement pstmt = con.prepareStatement(updateSQL);
        pstmt.setString(1, username);
        pstmt.setString(2, password);
        pstmt.setString(3, ssn);
        pstmt.executeUpdate();
        closeConnectionToCustomerDatabase();
    }


    public void closeConnectionToCustomerDatabase() throws SQLException {
        System.out.println("Connection closed");
        con.close();
    }

    private String getRandomizedString(String randomized) {
        if (System.getProperty("random") == null) {
            System.setProperty("random", "5");
        }
        return randomized + System.getProperty("random");
    }


}
