package steps.databaseSteps;

import cucumber.api.java.en.Given;
import steps.BaseSteps;
import utils.ConnectionManager;
import utils.TestProperties;

import java.sql.*;
import java.util.Properties;

public class MessagesDatabaseSteps extends BaseSteps {

    protected static Properties properties;

    static {
        properties = TestProperties.getInstance();
    }

    private Connection con = null;
    private Statement stmt = null;
    private ResultSet rs = null;

    public void connectToMessagesDatabase() {
        System.out.println("Connection created");
        con = ConnectionManager.getConnection(
                properties.getProperty("postgre.url"),
                properties.getProperty("postgre.message.username"),
                properties.getProperty("postgre.message.password"),
                properties.getProperty("postgre.driver.name")
        );
        System.out.println("Connected to:");
        System.out.println(properties.getProperty("postgre.url"));
        System.out.println("");
    }

    @Given("^I delete all message for case (.*) and debtorreference (.*) and prodsys (.*)$")
    public void iDeleteAllMessagesForCaseRef(String caseRef, String debtorRef, String prodSys) throws SQLException {
        String randomizedObligationRef = prodSys + "::CA" + caseRef + "::DE" + debtorRef;
        connectToMessagesDatabase();
        con.createStatement();
        int affectedrows = 0;
        String deleteMessages =
                "DELETE FROM " +
                        "message " +
                        "WHERE jsonb_extract_path_text(message, 'obligationReference') = ?";
        PreparedStatement pstmt = con.prepareStatement(deleteMessages);
        pstmt.setString(1, randomizedObligationRef);
        affectedrows = pstmt.executeUpdate();
        con.commit();
        System.out.println("All " + affectedrows + " messages for " + randomizedObligationRef + " are deleted!");
        closeConnectionToMessagesDatabase();
    }

    public void closeConnectionToMessagesDatabase() throws SQLException {
        System.out.println("Connection closed");
        con.close();
    }


    private String getRandomizedString(String randomized) {
        if (System.getProperty("random") == null) {
            System.setProperty("random", "5");
        }
        return randomized + System.getProperty("random");
    }
}