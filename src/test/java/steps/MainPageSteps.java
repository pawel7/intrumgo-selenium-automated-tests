package steps;

import com.relevantcodes.extentreports.LogStatus;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.*;
import utils.CustomExpectedConditions;
import utils.ReadTestDataFile;
import utils.Screenshots;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class MainPageSteps extends BaseSteps {

    MainPage mainPage;
    BasePage basePage;

    @Before
    public void before(Scenario scenario) throws Exception {
        Screenshots screen = new Screenshots();
        scenarioName = screen.getNameOfScenario(scenario);
        featureName = screen.getNameOfFeature(scenario);

        updateExtentTest(featureName, scenarioName);

        mainPage = new MainPage(driver, wait);
        basePage = new BasePage(driver, wait);
    }

//    @Then("^User is transferred to the restricted part$")
//    public void userIsTransferredToTheRestrictedPart() {
//        mainPage.checkIfElementsAreVisibleOnDesktop();
//    }

//    @Then("^Static cases elements are displayed for all cases")
//    public void userIsAbleToSeeStaticCasesElements(DataTable labels) {
//
//        List<Map<String, String>> data = labels.asMaps(String.class, String.class);
//        String caseLabel = data.get(0).get("LEFT_TO_PAY");
//        String activity = data.get(0).get("ACTIVITY");
//        String caseIdLabel = data.get(0).get("CASE_NAME");
//        mainPage.caseList().waitForElementPresents();
//
//        for (int i = 1; i <= mainPage.caseList().getItemCount(); i++) {
//            mainPage.assertThatContains(mainPage.caseCardID(i).getValue(), caseIdLabel, "Case ID is properly displayed");
//            mainPage.assertThatEquals(mainPage.caseAmountLeftToPayLabel(i).getValue(), caseLabel, "Left to pay label is displayed");
//            mainPage.assertThatEquals(mainPage.caseActivityLabel(i).getValue(), activity, "Activity label is displayed");
//            assertThat(mainPage.caseActivityEmailIcon(i).isDisplayed())
//                    .as("Activity icon1 is displayed").isTrue();
//            assertThat(mainPage.caseActivityHistoryIcon(i).isDisplayed())
//                    .as("Activity icon2 is displayed").isTrue();
//        }
//        extentTest.log(LogStatus.INFO, "Static cases elements are displayed for all cases");
//    }

//    @Then("^Static closed cases elements are displayed for all closed cases")
//    public void userIsAbleToSeeStaticClosedCasesElements(DataTable labels) {
//
//        List<Map<String, String>> data = labels.asMaps(String.class, String.class);
//        String caseLabel = data.get(0).get("LEFT_TO_PAY");
//        String caseIdLabel = data.get(0).get("CASE_NAME");
//        mainPage.caseList().waitForElementPresents();
//
//        for (int i = 1; i <= mainPage.caseList().getItemCount(); i++) {
//            mainPage.assertThatContains(mainPage.caseCardID(i).getValue(), caseIdLabel, "Case name label is wrong");
//            mainPage.assertThatEquals(mainPage.caseAmountLeftToPayLabel(i).getValue(), caseLabel, "Left to pay label is displayed");
//        }
//        extentTest.log(LogStatus.INFO, "Static cases elements are displayed for all cases");
//    }



//    @Then("^For case: (.*) company name: (.*) and left to pay amount: (.*) are displayed")
//    public void userIsAbleToSeeNonStaticCasesElements(int caseIDNumber, String companyName, String leftToPayAmount) {
//        wait.until(CustomExpectedConditions.pageIsLoaded());
//
//        mainPage.assertThatContains(mainPage.caseCardIDByID(caseIDNumber).getValue(), String.valueOf(caseIDNumber), "Case ID is properly displayed");
//        mainPage.assertThatContains(mainPage.caseClientNameById(caseIDNumber).getValue(), companyName, "Client name is properly displayed");
//        mainPage.assertThatContains(mainPage.caseAmountLeftToPayById(caseIDNumber).getValue(), leftToPayAmount, "Left to pay amount is properly displayed");
//
//        extentTest.log(LogStatus.INFO, "For case: " + caseIDNumber + "  company name: " + companyName
//                + " and left to pay amount: " + leftToPayAmount + " are displayed");
//    }

//    @Then("^For case: (.*) company name: (.*) are displayed limited")
//    public void userIsAbleToSeeNonStaticCasesElementsLimited(String caseIDNumber, String companyName) {
//        wait.until(CustomExpectedConditions.pageIsLoaded());
//
//        assertThat(mainPage.caseCardIDByID(Integer.valueOf(caseIDNumber)).getValue())
//                .as("Case ID is properly displayed").contains(caseIDNumber);
//        assertThat(mainPage.caseClientNameById(Integer.valueOf(caseIDNumber)).getValue())
//                .as("Client name is properly displayed").isEqualTo(companyName);
//    }
//
//    @Then("^For NO closed case: (.*) company name: (.*) are displayed")
//    public void userIsAbleToSeeNonStaticNOCasesElements(String caseIDNumber, String companyName) {
//        wait.until(CustomExpectedConditions.pageIsLoaded());
//
//        assertThat(mainPage.caseCardIDByID(Integer.valueOf(caseIDNumber)).getValue())
//                .as("Case ID is properly displayed").contains(caseIDNumber);
//        assertThat(mainPage.caseClientNameById(Integer.valueOf(caseIDNumber)).getValue())
//                .as("Client name is properly displayed").isEqualTo(companyName);
//
//
//        extentTest.log(LogStatus.INFO, "For case: " + caseIDNumber + "  company name: " + companyName
//                + "are displayed");
//    }
//
//    @Then("^For SE closed case: (.*) company name: (.*) and left to pay amount: (.*) are displayed")
//    public void userIsAbleToSeeNonStaticSECasesElements(String caseIDNumber, String companyName, String leftToPayAmount) {
//        wait.until(CustomExpectedConditions.pageIsLoaded());
//
//        assertThat(mainPage.caseCardIDByID(Integer.valueOf(caseIDNumber)).getValue())
//                .as("Case ID is properly displayed").contains(caseIDNumber);
//        assertThat(mainPage.caseClientNameById(Integer.valueOf(caseIDNumber)).getValue())
//                .as("Client name is properly displayed").isEqualTo(companyName);
//        assertThat(mainPage.caseAmountLeftToPayById(Integer.valueOf(caseIDNumber)).getValue())
//                .as("Left to pay amount is properly displayed").isEqualTo(leftToPayAmount);
//
//        extentTest.log(LogStatus.INFO, "For case: " + caseIDNumber + "  company name: " + companyName
//                + " and left to pay amount: " + leftToPayAmount + " are displayed");
//    }
//
//
//    @Then("^Total amount of cases is: (.*)")
//    public void userIsAbleToSeeCertainAmountOfCases(int numberOfCases) {
//        wait.until(CustomExpectedConditions.pageIsLoaded());
//
//        assertThat(mainPage.caseList().getItemCount())
//                .as("Total case amount is correct").isEqualByComparingTo(numberOfCases);
//
//        extentTest.log(LogStatus.INFO, "Cases amount is correct and closed cases are not displayed");
//    }

    @Then("^User clicks case (.*)")
    public void userIsNotAbleToClickCase(String caseIDNumber) {
        wait.until(CustomExpectedConditions.pageIsLoaded());
        mainPage.caseCard(caseIDNumber).click();
    }


    private String getRandomizedString(String randomized) {
        if (System.getProperty("random") == null) {
            System.setProperty("random", "5");
        }
        return randomized + System.getProperty("random");
    }

    @When("^User clicks pay now button$")
    public void userClicksPayNowButton() {
        mainPage.payNowButton().click();
        extentTest.log(LogStatus.INFO, "User clicked Pay now button");
    }

    @When("^User opens case number: (.*)$")
    public void userOpenCaseNumber(String caseNumber) {
        mainPage.caseCard(caseNumber).click();
        extentTest.log(LogStatus.INFO, "User opened case");
    }

    @When("^-param- User opens case number: (.*)$")
    public void userOpenCaseNumberWithParamn(String caseNumber) {
        mainPage.caseCard(caseNumber).click();
        extentTest.log(LogStatus.INFO, "User opened case");
    }

    @When("^-param- User opens closed case number: (.*)$")
    public void userOpenClosedCaseNumberWithParamn(String caseNumber) {
        mainPage.caseCard(caseNumber).click();
        extentTest.log(LogStatus.INFO, "User opened case");
    }

    @When("^User opens closed case number: (.*)$")
    public void userOpenClosedCaseNumberWithoutParamn(String caseNumber) {
        mainPage.caseCard(caseNumber).click();

        extentTest.log(LogStatus.INFO, "User opened case");
    }

    @And("^button Pay now is not displayed$")
    public void buttonSaysPayWithBankIsNotDisplayed() {
        assertThat(!mainPage.payNowButton().isPresents()).as("Pay now button is invisible").isTrue();
    }



//    @And("^User is able to see expanded chat$")
//    public void chatExpandedIsDisplayed() {
//        assertThat(mainPage.chatInputField().isDisplayed()).as("Chat expanded is invisible").isTrue();
//    }

}
