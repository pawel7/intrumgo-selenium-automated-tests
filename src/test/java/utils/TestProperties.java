package utils;

import java.io.FileInputStream;
import java.util.Properties;

public class TestProperties extends Properties {
    private static TestProperties instance = null;

    private TestProperties() {
    }

    public static TestProperties getInstance() {
        if (System.getProperty("env") == null)
            System.setProperty("env", "test");
        if (System.getProperties().getProperty("port") == null)
            System.setProperty("port", "");

        String propertyFile = System.getProperties().getProperty("env") + ".properties";
        if (instance == null) {
            try {
                instance = new TestProperties();
                FileInputStream in = new FileInputStream(propertyFile);
                instance.load(in);
                if ((System.getProperties().getProperty("port")).equals("")) {
                    instance.setProperty("application.port", System.getProperties().getProperty("port"));
                } else {
                    instance.setProperty("application.port", ":" + System.getProperties().getProperty("port"));
                }
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return instance;
    }
}
