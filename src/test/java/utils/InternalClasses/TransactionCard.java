package utils.InternalClasses;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TransactionCard {
    private String transactionDate;
    private String transactionIcon;
    private String transactionDescription;
}
