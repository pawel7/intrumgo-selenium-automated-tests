package utils.InternalClasses;


public class Payment {


    private String amount;
    private String currency;
    private String provider;
    private String status;
    private String merchant;


    public Payment(String amount, String currency, String provider, String status, String merchant) {
        this.amount = amount;
        this.currency = currency;
        this.provider = provider;
        this.status = status;
        this.merchant = merchant;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }


}
