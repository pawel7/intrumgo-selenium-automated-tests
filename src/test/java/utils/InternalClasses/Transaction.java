package utils.InternalClasses;


public class Transaction {
    public String productionSystemId;
    public String caseRef;
    public String amount;
    public String currency;
    public String paymentID;
    public String status;
    public String transcationDate;
    public String ID;

    public Transaction(String productionSystemId, String caseRef, String amount, String currency, String paymentID, String status, String transcationDate, String ID) {
        this.productionSystemId = productionSystemId;
        this.caseRef = caseRef;
        this.amount = amount;
        this.currency = currency;
        this.paymentID = paymentID;
        this.status = status;
        this.transcationDate = transcationDate;
        this.ID = ID;
    }

    public String getProductionSystemId() {
        return productionSystemId;
    }

    public String getID() {
        return ID;
    }

    public String getCaseRef() {
        return caseRef;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPaymentID() {
        return paymentID;
    }

    public String getStatus() {
        return status;
    }

    public String getTranscationDate() {
        return transcationDate;
    }


}
