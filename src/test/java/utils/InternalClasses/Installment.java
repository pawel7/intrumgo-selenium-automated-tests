package utils.InternalClasses;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

@Builder
@Data
public class Installment {
    int installmentId;
    double amount;
    String dueDate;
    String currency;

    public String getAmountConverted() {
        return PaymentPlanCardTest.convertNumberWithThousandsSeperator(amount) + currency;
    }

    public String getMonthAsText() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-mm-DD");
        LocalDate today = LocalDate.parse(dueDate);
        int month = today.getMonthValue();
        return Month.of(month).name();
    }

}