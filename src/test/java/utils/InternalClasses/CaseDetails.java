package utils.InternalClasses;

import lombok.Builder;
import lombok.Data;

import static utils.InternalClasses.PaymentPlanCardTest.convertNumberWithThousandsSeperator;

@Builder
@Data
public class CaseDetails {
    private String caseNumber;
    private String clientName;
    private String amountLeftToPay;
    private String currency;

    public String getLeftToPayWithCurrency() {
        return convertNumberWithThousandsSeperator(Double.parseDouble(amountLeftToPay)) + currency;
    }
}
