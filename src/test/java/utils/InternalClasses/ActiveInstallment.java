package utils.InternalClasses;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ActiveInstallment {
    private String month;
    private String status;
    private String amount;
    private String dueDate;
    private String totalLeftToPayLabel;
    private String totalLeftToPayAmount;
    private String skipUpcomingFeeHeading;
    private String skipUpcomingFeeLabel;
}
