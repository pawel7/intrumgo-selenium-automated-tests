package utils.InternalClasses;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class InvoiceRow {
    private String invoiceNr;
    private String sentDate;
    private String dueDate;
    private String original;
    private String remaining;
    private String interest;
}
