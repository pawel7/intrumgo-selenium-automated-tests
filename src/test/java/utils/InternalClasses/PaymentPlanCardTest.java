package utils.InternalClasses;

import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.json.JSONArray;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

@Log4j
@Builder
@Data
public class PaymentPlanCardTest {
    private int installmentDuration;
    private String installmentType;
    private Double paymentAmount;
    private Double feeAmount;
    private Double setupAmount;
    private Double totalAmount;
    private String feeLabel;
    private String setupLabel;
    private String totalLabel;
    private String dueDate;
    private String currency;
    private String actualInstallmentDuration;
    private String actualPaymentAmount;
    private String actualFeeAmount;
    private String actualSetupAmount;
    private String actualTotalAmount;
    private String actualDueDate;
    private JSONArray activeInstallments;

    public static String convertNumberWithThousandsSeperator(Double number) {
        DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols();
        unusualSymbols.setGroupingSeparator(' ');
        unusualSymbols.setDecimalSeparator('.');
        DecimalFormat myFormatter = new DecimalFormat("###, ###.00", unusualSymbols);
        String output = myFormatter.format(number);
        return output;
    }

    public String getInstallmenDurationExpected() {
        String periodType = "";
        if (installmentType.equals("MONTHLY") || installmentType.equals("MONTH")) periodType = "MONTHS";
        return installmentDuration + " " + periodType;
    }

    public String getPaymentAmountExpected() {
        String periodType = "";
        if (installmentType != null && (installmentType.equals("MONTHLY") || installmentType.equals("MONTH")))
            periodType = "month";
        else {
            periodType = "month";
        }
        return convertNumberWithThousandsSeperator(paymentAmount) + currency + " / " + periodType;
    }

    public String getPaymentAmountWithoutMonthExpected() {
        return convertNumberWithThousandsSeperator(paymentAmount) + currency;
    }

    public String getSetupAmountExpected() {
        return convertNumberWithThousandsSeperator(setupAmount) + currency;
    }

    public String getFeeAmountExpected() {
        return convertNumberWithThousandsSeperator(feeAmount) + currency;
    }

    public String getTotalAmountExpected() {
        return convertNumberWithThousandsSeperator(totalAmount) + currency;
    }

    public String getPlanLengthExpected() {
        String periodType = "";
        if (installmentType != null && (installmentType.equals("MONTHLY") || installmentType.equals("MONTH")))
            periodType = "months";
        else {
            periodType = "months";
        }
        return installmentDuration + " " + periodType + " plan";
    }

    public List<Installment> getActiveInstallments() {
        List<Installment> installments = new ArrayList<>();
        for (int i = 0; i < activeInstallments.length(); i++) {
            Installment installment = Installment.builder()
                    .installmentId(activeInstallments.getJSONObject(i).getInt("installmentId"))
                    .amount(activeInstallments.getJSONObject(i).getJSONObject("installmentAmount").getDouble("amount"))
                    .dueDate(activeInstallments.getJSONObject(i).getString("dueDate"))
                    .currency(activeInstallments.getJSONObject(i).getJSONObject("installmentAmount").getString("currency"))
                    .build();
            installments.add(installment);
        }
        log.info("Active installment list size is " + installments.size());
        return installments;
    }


}

