package utils.InternalClasses;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class MessageCard {
    private String sender;
    private String messageType;
    private String date;
}
