package utils.InternalClasses;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PaymentPlanCard {
    private String installmentDuration;
    private String paymentAmount;
    private String feeAmount;
    private String setupAmount;
    private String totalAmount;
    private String feeLabel;
    private String setupLabel;
    private String totalLabel;
    private String dueDate;
}
