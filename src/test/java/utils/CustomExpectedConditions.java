package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class CustomExpectedConditions {


    public static ExpectedCondition<Boolean> pageIsLoaded(
    ) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                return js.executeScript("return document.readyState").equals("complete");
            }
        };
    }

    public static ExpectedCondition<Boolean> ajaxIsFinished(
    ) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                System.out.println("Waiting until Page is loaded...: " + js.executeScript("return jQuery.active"));
                return js.executeScript("return jQuery.active").equals(0L);
            }
        };
    }

    public static ExpectedCondition<Boolean> elementTextNotEmpty(final By by) {

        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    String elementText = driver.findElement(by).getText();
                    return !elementText.isEmpty();
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("element text is not empty", by);
            }
        };
    }

    public static ExpectedCondition<Boolean> tabCountToBeOpened(final int count) {
        return new ExpectedCondition<Boolean>() {
            int tabCount;

            @Override
            public Boolean apply(WebDriver driver) {
                tabCount = driver.getWindowHandles().size();
                return tabCount != 0 && tabCount == count;
            }

            @Override
            public String toString() {
                return String.format("Tab count to be \"%s\". Current tab count : \"%s\"", count, tabCount);
            }
        };
    }


}

