package utils;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Log4j
public class BrowserProperties extends DesiredCapabilities {

    private static BrowserProperties desiredCapabilities = null;

    private BrowserProperties() {
    }

    public static BrowserProperties getBrowser() {

        if (System.getProperty("browser") == null) {
            System.setProperty("browser", "chrome");
        }

        if (desiredCapabilities == null) {
            desiredCapabilities = new BrowserProperties();
            switch (System.getProperty("browser")) {
                case "firefox":
                    setCapabilities("firefox");
                    log.info("Setting browser to firefox");
                    break;
                case "chrome":
                    setCapabilities("chrome");
                    log.info("Setting browser to chrome");
                    break;
                case "IE":
                    setCapabilities("ie");
                    break;
                default:
                    setCapabilities("chrome");
                    break;
            }
        }
        return desiredCapabilities;
    }

    private static void setCapabilities(String browserName) {

        if (browserName.equals("chrome")) {
            log.info("Setting capabilities to chrome");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-web-security");
            Map<String, Object> prefs = new HashMap<String, Object>();
            String path = "C:+" + File.separator + "selenium";
            prefs.put("download.default_directory", path);
            options.setExperimentalOption("prefs", prefs);
            desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
        }

        if (browserName.equals("firefox")) {
            log.info("Setting capabilities to firefox");
            FirefoxOptions options = new FirefoxOptions();
            options.setCapability("acceptInsecureCerts", true);
            desiredCapabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
        }

        desiredCapabilities.setCapability("browserName", browserName);
        desiredCapabilities.setCapability("version", "");
        desiredCapabilities.setCapability("platform", "");
    }
}
