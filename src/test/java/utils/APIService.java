package utils;

import cucumber.api.DataTable;
import lombok.extern.log4j.Log4j;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.InternalClasses.CaseDetails;
import utils.InternalClasses.PaymentPlanCardTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

@Log4j
public class APIService {
    private static final MediaType JSON;
    protected static Properties properties = TestProperties.getInstance();

    static {
        JSON = MediaType.parse("application/json; charset=utf-8");
    }

    private OkHttpClient client = new OkHttpClient();
    private String credentialRekonds = Credentials.basic(properties.getProperty("rekondis.api.username"),
            properties.getProperty("rekondis.api.password"));
    private String credentialNova = Credentials.basic(properties.getProperty("nova.api.username"),
            properties.getProperty("nova.api.password"));
    private String credentialNomos = Credentials.basic(properties.getProperty("nomos.api.username"),
            properties.getProperty("nomos.api.password"));

    private String getKeyCloakAuthToken(String clientID, String clientSecret) throws IOException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(properties.getProperty("keycloak.ip"))
                .port(Integer.parseInt(properties.getProperty("keycloak.port")))
                .addPathSegment("auth")
                .addPathSegment("realms")
                .addPathSegment("iw-services-test")
                .addPathSegment("protocol")
                .addPathSegment("openid-connect")
                .addPathSegment("token")
                .build();

        RequestBody requestBody = new FormBody.Builder()
                .add("grant_type", "client_credentials")
                .add("client_id", clientID)
                .add("client_secret", clientSecret)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(requestBody)
                .build();

        Response response = client.newCall(request).execute();
        JSONObject Jobject = new JSONObject(response.body().string());
        System.out.println(Jobject.getString("access_token"));
        return Jobject.getString("access_token");
    }

    public void postMessageFromProdSystem(String obligationReference, String clientID, String clientSecret) throws IOException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(properties.getProperty("nomos.api.ip"))
                .port(Integer.parseInt(properties.getProperty("nomos.api.port")))
                .addPathSegment("cases")
                .addPathSegment(obligationReference)
                .addPathSegment("messages")
                .build();

        RequestBody requestBody = RequestBody
                .create(JSON, productionSystemJSON(obligationReference,
                        "This is message from auto tests " + obligationReference));

        Request request = new Request.Builder()
                .url(url)
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + this.getKeyCloakAuthToken(clientID, clientSecret))
                .post(requestBody)
                .build();

        Response response = client.newCall(request).execute();
        System.out.println(response);
    }

    public void deletePaymentPlanToNovaForDebtor(String debtorID, String caseID, String novaType) throws IOException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(properties.getProperty("nova.api.ip"))
                .port(Integer.parseInt(properties.getProperty("nova.api.port")))
                .addPathSegment("api")
                .addPathSegment(novaType)
                .addPathSegment("cases")
                .addPathSegment(caseID)
                .addPathSegment("payment-plans")
                .addQueryParameter("debtorId", debtorID)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", credentialNova)
                .delete()
                .build();

        client.newCall(request).execute();
    }

    public void deletePaymentPlanToRekondisForDebtor(String debtorID, String caseID) throws IOException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(properties.getProperty("rekondis.api.ip"))
                .port(Integer.parseInt(properties.getProperty("rekondis.api.port")))
                .addPathSegment("rks-nomos-api")
                .addPathSegment("cases")
                .addPathSegment(caseID)
                .addPathSegment("payment-plan")
                .addQueryParameter("debtorId", debtorID)
                .addQueryParameter("keyId", "*")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", credentialRekonds)
                .delete()
                .build();

        Response response = client.newCall(request).execute();
        assertThat(response.code()).as("Response code is 200").isEqualTo(200);
    }

    private String productionSystemJSON(String obligationReference, String message) {
        return new JSONObject()
                .put("type", "PRODUCTION_SYSTEM_FREE_TEXT")
                .put("sender", "PRODUCTION_SYSTEM")
                .put("obligationReference", obligationReference)
                .put("text", message).toString();
    }

    private String createOrderPOSTJson(DataTable values) {
        List<List<String>> data = values.raw();
        return new JSONObject()
                .put("reference", data.get(0).get(1))
                .put("payerReference", data.get(1).get(1))
                .put("merchant", data.get(2).get(1))
                .put("validUntil", data.get(3).get(1))
                .put("amount", new JSONObject()
                        .put("amount", data.get(4).get(1))
                        .put("currency", data.get(5).get(1)))
                .put("items", new JSONArray().put(new JSONObject().put("amount", new JSONObject()
                        .put("amount", data.get(6).get(1))
                        .put("currency", data.get(7).get(1)))
                        .put("details", data.get(8).get(1))
                ))
                .put("details", data.get(9).get(1))
                .put("theme", data.get(10).get(1))
                .toString();
    }

    public String postGenericOrderFromProductionSystem(DataTable values) throws IOException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("http")
                .host(properties.getProperty("nomos.test.ip"))
                .addPathSegment("order-service-api")
                .addPathSegment("orders")
                .build();

        RequestBody requestBody = RequestBody
                .create(JSON, createOrderPOSTJson(values));

        Request request = new Request.Builder()
                .url(url)
                .header("Content-Type", "application/json")
                .header("Authorization", credentialNomos)
                .post(requestBody)
                .build();

        Response response = client.newCall(request).execute();
        assertThat(response.code()).as("Response code is 201").isEqualTo(201);
        JSONObject jsonObject = new JSONObject(response.body().string());
        return jsonObject.getString("id");

    }


    public List<PaymentPlanCardTest> getPaymentPlanOptions(String caseId, String customerId, String startDate, String status, String prodSystem) throws IOException {
        Request request = swedenPaymentPlanRequestBuilder(prodSystem, caseId, customerId, status, startDate);
        Response response = client.newCall(request).execute();
        assertThat(response.code()).as("Response code is 200").isEqualTo(200);
        JSONArray jsonArray = new JSONArray(response.body().string());
        log.info(jsonArray.toString());
        List<PaymentPlanCardTest> paymentPlanOptions = new ArrayList<PaymentPlanCardTest>();
        for (int i = 0; i < jsonArray.length(); i++) {
            PaymentPlanCardTest paymentPlanCard = PaymentPlanCardTest.builder()
                    .paymentAmount(jsonArray.getJSONObject(i).getJSONObject("installmentAmount").getDouble("amount"))
                    .currency(jsonArray.getJSONObject(i).getJSONObject("installmentAmount").getString("currency"))
                    .installmentDuration(jsonArray.getJSONObject(i).getInt("installmentCount"))
                    .installmentType(jsonArray.getJSONObject(i).getString("installmentPeriod"))
                    .feeAmount(jsonArray.getJSONObject(i).getJSONObject("fixedCosts").getDouble("amount"))
                    .totalAmount(jsonArray.getJSONObject(i).getJSONObject("totalAmountDue").getDouble("amount"))
                    .dueDate(jsonArray.getJSONObject(i).getJSONArray("installments").getJSONObject(0).getString("dueDate"))
                    .setupAmount(jsonArray.getJSONObject(i).getJSONObject("advisingCosts").getDouble("amount"))
                    .activeInstallments(jsonArray.getJSONObject(i).getJSONArray("installments")
                    )
                    .build();
            paymentPlanOptions.add(paymentPlanCard);
        }
        return paymentPlanOptions;
    }


    public CaseDetails getCaseDetails(String caseId, String prodSys) throws IOException {
        HttpUrl.Builder newUrlBuilder = new HttpUrl.Builder();
        Request.Builder request = new Request.Builder();
        getCorrectProdSysPath(newUrlBuilder, request, prodSys);
        newUrlBuilder
                .scheme("http")
                .addPathSegment("cases")
                .addPathSegment(caseId);
        if (prodSys.contains("rekondis")) {
            newUrlBuilder.addQueryParameter("keyId", "*");
        }
        newUrlBuilder.build();
        System.out.println(newUrlBuilder);
        log.info(newUrlBuilder);

        Response response = client.newCall(request.url(newUrlBuilder.build()).get().build()).execute();
        assertThat(response.code()).as("Response code is 200").isEqualTo(200);

        JSONObject jsonObject = new JSONObject(response.body().string());
        log.info(jsonObject.toString());
        CaseDetails caseDetails = CaseDetails.builder()
                .caseNumber(String.valueOf(jsonObject.getInt("caseId")))
                .clientName(jsonObject.getString("clientName"))
                .amountLeftToPay(String.valueOf(jsonObject.getJSONObject("amountDue").getDouble("amount")))
                .currency(jsonObject.getJSONObject("amountDue").getString("currency"))
                .build();

        return caseDetails;
    }

    private Request swedenPaymentPlanRequestBuilder(String prodSys, String caseId, String customerId, String status, String startDate) {
        HttpUrl.Builder newUrlBuilder = new HttpUrl.Builder();
        Request.Builder request = new Request.Builder();
        newUrlBuilder.scheme("http");
        getCorrectProdSysPath(newUrlBuilder, request, prodSys);

        newUrlBuilder.addPathSegment("cases")
                .addPathSegment(caseId)
                .addPathSegment("payment-plans")
                .addQueryParameter("debtorNumber", customerId);
        if (prodSys.equals("rekondis")) {
            newUrlBuilder.addQueryParameter("keyId", "*");
        }

        newUrlBuilder.addQueryParameter("status", status)
                .addQueryParameter("startDate", startDate);

        log.info(newUrlBuilder.build());
        request.url(newUrlBuilder.build());
        return request.get().build();
    }

    private void getCorrectProdSysPath(HttpUrl.Builder httpUrl, Request.Builder request, String prodSys) {
        if (prodSys.equals("rekondis")) {
            httpUrl.host(properties.getProperty("rekondis.api.ip"))
                    .port(Integer.parseInt(properties.getProperty("rekondis.api.port")))
                    .addPathSegment("rks-nomos-api");
            request.header("Authorization", credentialRekonds);
        }
        if (prodSys.equals("nova-pd")) {
            httpUrl.host(properties.getProperty("nova.api.ip"))
                    .port(Integer.parseInt(properties.getProperty("nova.api.port")))
                    .addPathSegment("api")
                    .addPathSegment("nova-pd");
            request.header("Authorization", credentialNova);
        }
        if (prodSys.equals("nova-ds")) {
            httpUrl.host(properties.getProperty("nova.api.ip"))
                    .port(Integer.parseInt(properties.getProperty("nova.api.port")))
                    .addPathSegment("api")
                    .addPathSegment("nova-ds");
            request.header("Authorization", credentialNova);
        }
        if (prodSys.equals("L24FakeBE")) {
            httpUrl.host(properties.getProperty("L24.api.ip"))
                    .addPathSegment("DebtorProxyApiFakeBE")
                    .addPathSegment("api");
        }

    }

    public PaymentPlanCardTest getActivePaymentPlan(String caseId, String customerId, String startDate, String status, String prodSystem) throws IOException {
        Request request = swedenPaymentPlanRequestBuilder(prodSystem, caseId, customerId, status, startDate);
        Response response = client.newCall(request).execute();
        assertThat(response.code()).as("Response code is 200").isEqualTo(200);
        JSONArray jsonArray = new JSONArray(response.body().string());
        log.info(jsonArray.toString());
        JSONObject jsonObject = jsonArray.getJSONObject(0);

        PaymentPlanCardTest paymentPlanCard = PaymentPlanCardTest.builder()
                .paymentAmount(jsonObject.getJSONObject("installmentAmount").getDouble("amount"))
                .currency(jsonObject.getJSONObject("installmentAmount").getString("currency"))
                .installmentDuration(jsonObject.getInt("installmentCount"))
                .installmentType(jsonObject.getString("installmentPeriod"))
                .activeInstallments(jsonObject.getJSONArray("installments"))
                .totalAmount(jsonObject.getJSONObject("totalAmountDue").getDouble("amount"))
                .build();

        return paymentPlanCard;
    }

    private Request norwayPaymenplanRequestBuilder(String prodSys, String caseId, String customerId, String type) {
        HttpUrl.Builder newUrlBuilder = new HttpUrl.Builder();
        Request.Builder request = new Request.Builder();
        newUrlBuilder.scheme("http");
        getCorrectProdSysPath(newUrlBuilder, request, prodSys);

        newUrlBuilder.addPathSegment("cases")
                .addPathSegment(caseId)
                .addPathSegment(type)
                .addQueryParameter("Country", "Norway")
                .addQueryParameter("DebtorId", customerId);

        log.info(newUrlBuilder.build());
        request.url(newUrlBuilder.build());
        return request.get().build();

    }


    public PaymentPlanCardTest getNorwayActivePaymentPlan(String prodSys, String caseId, String customerId, String type) throws IOException {
        Request request = norwayPaymenplanRequestBuilder(prodSys, caseId, customerId, type);
        Response response = client.newCall(request).execute();
        assertThat(response.code()).as("Response code is 200").isEqualTo(200);
        JSONObject jsonObject = new JSONObject(response.body().string());
        log.info(jsonObject.toString());

        PaymentPlanCardTest paymentPlanCard = PaymentPlanCardTest.builder()
                .paymentAmount(jsonObject.getJSONObject("installmentAmount").getDouble("amount"))
                .currency(jsonObject.getJSONObject("installmentAmount").getString("currency"))
                .build();

        return paymentPlanCard;
    }

    public PaymentPlanCardTest getNorwayPaymentPlanOptions(String prodSys, String caseId, String customerId, String type, int number) throws IOException {
        Request request = norwayPaymenplanRequestBuilder(prodSys, caseId, customerId, type);
        Response response = client.newCall(request).execute();
        assertThat(response.code()).as("Response code is 200").isEqualTo(200);
        JSONObject jsonObject = new JSONObject(response.body().string());
        log.info(jsonObject.toString());
        jsonObject = jsonObject.getJSONArray("paymentPlanProposals").getJSONObject(number - 1);
        PaymentPlanCardTest paymentPlanCard = PaymentPlanCardTest.builder()
                .paymentAmount(jsonObject.getJSONObject("installmentAmount").getDouble("amount"))
                .currency(jsonObject.getJSONObject("installmentAmount").getString("currency"))
                .installmentDuration(jsonObject.getInt("numberOfInstallments"))
                .build();

        return paymentPlanCard;
    }
}
