package utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import lombok.extern.log4j.Log4j;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Log4j
public class DriverSingleton {
    public static TestProperties properties;
    public static BrowserProperties browserProperties;
    private static DriverSingleton instance;
    private static RemoteWebDriver driver;
    private static ExtentReports extent;
    private static ExtentTest extentTest;

    private static Map<String, ExtentTest> extentTestDictionary;


    private DriverSingleton() {
        try {
//            System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver_win32\\chromedriver.exe");
//            driver = new ChromeDriver();
            driver = new RemoteWebDriver(new URL(getSeleniumGridURL()), browserProperties.getBrowser());
            String reportsFileToSave = System.getProperty("user.dir") + File.separator + properties.getProperty("screenshots.folder1") + File.separator + properties.getProperty("screenshots.folder2") + File.separator + "report.html";
            extent = new ExtentReports(reportsFileToSave, false);
            extentTestDictionary = new HashMap<String, ExtentTest>();

            //ToDo: Manage the global waits
            driver.manage().window().maximize();
//            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(45, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(45, TimeUnit.SECONDS);
            printOutBrowserInfo();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static DriverSingleton getInstance() {
        properties = TestProperties.getInstance();
        if (instance == null) {
            instance = new DriverSingleton();
        }
        return instance;
    }

    public static RemoteWebDriver getDriver() {
        return driver;
    }

    public static ExtentTest getExtentTest(String featureName, String scenarioName) {
        String extentTestKey = featureName + "//" + scenarioName;
        if (extentTestDictionary.containsKey(extentTestKey)) {
            extentTest = extentTestDictionary.get(extentTestKey);
        } else {
            extentTest = extent.startTest(extentTestKey, scenarioName);
            extentTestDictionary.put(extentTestKey, extentTest);
        }
        return extentTest;
    }

    public static ExtentReports getExtent() {
        return extent;
    }

    public static void destroySingletonInstance() {
        instance = null;
    }

    public static String getSeleniumGridURL() {
        if (System.getProperty("test.env") == null) {
            return properties.getProperty("selenium.grid.local");
        }
        return properties.getProperty("selenium.grid.server");
    }

    private static void printOutBrowserInfo() {
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        log.info("Browser: " + cap.getBrowserName());
        log.info("Browser version: " + cap.getVersion());
        log.info("Browser platform: " + cap.getPlatform());
    }
}
