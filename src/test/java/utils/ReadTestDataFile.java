package utils;

import lombok.extern.log4j.Log4j;
import org.json.JSONObject;
import steps.BaseSteps;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Log4j
public class ReadTestDataFile extends BaseSteps {

    public static List<String> selectedSubLanguageList = new ArrayList<String>();
    public static String mainLanguage;

    //ToDo temporary solution. Required to add proper exception message in case json pointer path is wrong

    /**
     * @param key - String input. If String starts with '@' sign, then the value will be fetched from test data json file. To fetch the test data from json required to use the json pointer.
     *            If String is not starts with '@' sign, then original value will be used.
     *            Example: String '@/test/key' -> Data will be searched in test data file and return the value
     *            Example: String 'Test' -> original value will be returned ('Test')
     * @return String - Test data value from json or original value
     * @throws Exception - 1. File not found exception (in case test data file is not found). 2. Null pointer exception in case json pointer path is wrong
     */
    public String getDataFromFile(String key) {
        String result;
        if (key.startsWith("@")) {
            JSONObject jsonObject = null;
            // File reader implementation
            // Try/catch required to handle the exception (to not implement the exception signature in the whole project)
            String testDataFileRelativePath = getTestDataSourceFileName();
            try {
                jsonObject = new JSONObject(new String(Files.readAllBytes(Paths.get(testDataFileRelativePath))));
            } catch (Exception e) {
                log.error("Test data json file is not found");
            }
            result = jsonObject.query(key.substring(1)).toString();
        } else {
            result = key;
        }

        log.info("Original value: " + key + ", Converted value: " + result);
        return result;
    }

    /////////////////////
    /// Helper method ///
    /////////////////////

    /**
     * Method to get the translation file name according to main and sub language
     *
     * @return String test data file name
     */
    private String getTestDataSourceFileName() {
        String testDataFileName = null;
        switch (mainLanguage) {
            case "se":
                testDataFileName = getTestDataFileRelativePath("Sweden", "TestData_SE.json");
                break;
            case "no":
                testDataFileName = getTestDataFileRelativePath("Norway", "TestData_NO.json");
                break;
            case "at":
                testDataFileName = getTestDataFileRelativePath("Austria", "TestData_AT.json");
                break;
            case "es":
                testDataFileName = getTestDataFileRelativePath("Spain", "TestData_ES.json");
                break;
            default:
                log.error("Test data file is not defined");
        }
        log.info("Main language is " + mainLanguage);
        return testDataFileName;
    }

    /**
     * Method to generate the test data file relative path
     *
     * @param folderName Folder where the file is located
     * @param fileName
     * @return String test data file relative path
     */
    private String getTestDataFileRelativePath(String folderName, String fileName) {
        List<String> test = new ArrayList<String>();
        test.add("src");
        test.add("test");
        test.add("resources");
        test.add("testData");
        test.add(folderName);
        test.add(fileName);

        String result = String.join(File.separator, test);
        log.info("Test data file relative path is " + result);
        return result;
    }

}
