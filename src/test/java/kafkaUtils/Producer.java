package kafkaUtils;

import lombok.extern.log4j.Log4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.json.JSONObject;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

@Log4j
public class Producer {
    private final KafkaProducer<Integer, String> producer;
    private final String topic;
    private final static String BOOTSTRAP_SERVERS = "http://sdcsrv092";
    private final static int KAFKA_SERVER_PORT = 29092;
    private final static String CLIENT_ID = "message-service-test";
    public final static String TOPIC = "customer-message-reply-sent-test-3020";

    public Producer(String topic) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS + ":" + KAFKA_SERVER_PORT);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, CLIENT_ID);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.IntegerSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<>(props);
        this.topic = topic;
    }

    public void sendMessage(JSONObject messageJSON) throws ExecutionException, InterruptedException {
        String messageStr = messageJSON.toString();
        RecordMetadata metadata = producer.send(new ProducerRecord<>(topic, messageStr)).get();
        log.info("Sent message is " + metadata.toString());
    }

}
