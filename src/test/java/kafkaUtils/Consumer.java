/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kafkaUtils;

import kafka.utils.ShutdownableThread;
import lombok.extern.log4j.Log4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Log4j
public class Consumer extends ShutdownableThread {
    private final KafkaConsumer<Integer, String> consumer;
    private final String topic;
    private final static String BOOTSTRAP_SERVERS = "http://sdcsrv092";
    private final static int KAFKA_SERVER_PORT = 29092;
    private final static String GROUP_ID = "export-service-messages-test";

    public Consumer(String topic) {
        super("KafkaConsumerExample", false);
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS + ":" + KAFKA_SERVER_PORT);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "300000");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.IntegerDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        consumer = new KafkaConsumer<>(props);
        this.topic = topic;
    }


    public List<JSONObject> getMessage() {
        consumer.subscribe(Collections.singletonList(this.topic));
        boolean flag = true;
        int recordIndex = 0;
        List<JSONObject> list = new ArrayList<>();
        int i = 0;

        while (i < 50) {
            ConsumerRecords<Integer, String> records = consumer.poll(1);
            TopicPartition topicPartitions = new TopicPartition(this.topic, 0);
            if (flag) {
                consumer.seekToEnd(Stream.of(topicPartitions).collect(toList()));
                Map offsets = consumer.endOffsets(Stream.of(topicPartitions).collect(toList()));
                consumer.seek(topicPartitions, (Long) offsets.get(topicPartitions) - 1);
                flag = false;
            }
            log.info("Kafka record count: " + records.count());

            for (ConsumerRecord<Integer, String> record : records) {
                log.info("Kafka record index: " + recordIndex + ", Record value: " + record.value());
                recordIndex++;
                list.add(new JSONObject(record.value()));
            }
            i++;
        }
        return list;
    }

    @Override
    public void doWork() {

    }
}