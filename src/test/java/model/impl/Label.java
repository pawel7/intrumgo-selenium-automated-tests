package model.impl;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Log4j
public class Label extends UIElement {
    public Label(WebDriver driver, By by) {
        super(driver, by);
    }

    /**
     * Override required due to following issue
     * If label im empty, then it can be not visible
     *
     * @return
     */
    @Override
    public String getValue() {
        waitPageIsLoaded();
        waitForElementPresents();
        moveToElement();
        String result = getElement().getText();
        if (!result.isEmpty()) {
            waitForElementVisible();
        }
        log.info("Text content from UI: " + result);
        return result;

    }

    public String getCssValue(String value) {
        waitPageIsLoaded();
        waitForElementPresents();
        moveToElement();
        String cssValue = getElement().getCssValue(value);
        log.info("CSS value is " + cssValue);
        return cssValue;
    }

}
