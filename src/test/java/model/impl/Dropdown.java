package model.impl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import static org.assertj.core.api.Assertions.assertThat;

public class Dropdown extends UIElement {
    public Dropdown(WebDriver driver, By by) {
        super(driver, by);
    }

    public void selectByText(String str) {
        waitAndClick();
        getParentElement().findElement(By.xpath("//*[self::span or self::li][contains(text(), '" + readTestDataFile.getDataFromFile(str) + "')]")).click();
        waitPageIsLoaded();
        assertThat(getValue()).as("Wrong dropdown option is selected").isEqualTo(readTestDataFile.getDataFromFile(str));
    }

    public void selectByOption(String str) {
        Select select = new Select(getElement());
        select.selectByValue(readTestDataFile.getDataFromFile(str));
    }

    // ToDo temporary solution
//    @Override
//    public String getValue() {
//        return getParentElement().findElement(By.xpath("..")).findElement(By.cssSelector("label")).getText();
//    }

}
