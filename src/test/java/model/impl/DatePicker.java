package model.impl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class DatePicker extends UIElement {
    public DatePicker(WebDriver driver, By by) {
        super(driver, by);
    }

    private By calendarMonth = By.cssSelector("span.ui-datepicker-month");
    private By calendarYear = By.cssSelector("span.ui-datepicker-year");
    private By nextMonth = By.cssSelector("span.pi-chevron-right");
    private By previousMonth = By.cssSelector("span.pi-chevron-left");

    public void open() {
        waitAndClick();
    }

    /**
     * @param date - String input. User can pass date in format ddMMyyyy or in format "today {sign} x" where sign is
     *             + or - and x number of days.
     *             <h3>Examples:</h3>
     *             <li>And User selects particular date - 20052019</li>
     *             <li>And User selects particular date : today - 5</li>
     *             <li>And User selects particular date : today + 5</li>
     */
    public void selectDate(String date) {
        open();
        LocalDate dateTime = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
        if (date.contains("today")) {
            if (date.contains("-")) dateTime = LocalDate.now().minusDays(getDayNumberFromDate(date));
            else if (date.contains("+")) dateTime = LocalDate.now().plusDays(getDayNumberFromDate(date));
            else {
                dateTime = LocalDate.now();
            }
        } else dateTime = LocalDate.parse(date, formatter);
        dateTime.getMonth().getValue();
        dateTime.getYear();
        selectCorrectYear(dateTime);
        selectCorrectMonth(dateTime);
        clickOnDay(dateTime);
    }

    private int getDayNumberFromDate(String date) {
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(date);
        while (m.find()) {
            return Integer.parseInt(m.group());
        }
        return 0;
    }

    private void selectCorrectYear(LocalDate dateTime) {
        if (dateTime.getYear() > getUIYearAsNumber()) {
            while (getUIYearAsNumber() != dateTime.getYear()) {
                getParentElement().findElement(nextMonth).click();
            }
        }
        if (dateTime.getYear() < getUIYearAsNumber()) {
            while (getUIYearAsNumber() != dateTime.getYear()) {
                getParentElement().findElement(previousMonth).click();
            }
        }
    }

    private void clickOnDay(LocalDate dateTime) {
        getParentElement().findElement(By.xpath("//td[normalize-space()='" + dateTime.getDayOfMonth() + "']")).click();
    }

    private void selectCorrectMonth(LocalDate dateTime) {
        int monthInUI = getUIMonthAsNumber();
        if (monthInUI > dateTime.getMonth().getValue()) {
            for (int i = 0; i < monthInUI - dateTime.getMonth().getValue(); i++) {
                getParentElement().findElement(previousMonth).click();
            }
        }
        if (monthInUI < dateTime.getMonth().getValue()) {
            for (int i = 0; i < dateTime.getMonth().getValue() - monthInUI; i++) {
                getParentElement().findElement(nextMonth).click();
            }
        }
    }

    //ToDo: Add multiple language support. Currently works for English only.
    private int getUIMonthAsNumber() {
        String monthName = getParentElement().findElement(calendarMonth).getText();
        return Month.valueOf(monthName.toUpperCase()).getValue();
    }

    private int getUIYearAsNumber() {
        return Integer.parseInt(getParentElement().findElement(calendarYear).getText());
    }

}
