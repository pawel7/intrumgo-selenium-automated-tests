package model.impl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TextBox extends UIElement {
    public TextBox(WebDriver driver, By by) {
        super(driver, by);
    }

    public void setValue(String str) {
        fillTextBox(str);
    }

    public void sendKeys(String str) {
        getElement().sendKeys(str);
    }

}
