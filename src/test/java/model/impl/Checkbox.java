package model.impl;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Log4j
public class Checkbox extends UIElement {
    public Checkbox(WebDriver driver, By by) {
        super(driver, by);
    }

    public void check() {
        if (!isChecked()) {
            getElement().click();
        }
        assertThat(isChecked()).as("Checkbox is checked").isTrue();
    }

    public void uncheck() {
        if (isChecked()) {
            getElement().click();
        }
        assertThat(isChecked()).as("Checkbox is not checked").isFalse();
    }

    public boolean isChecked() {
        waitEverythingIsLoaded();
        boolean checked = getElement().isSelected();
        log.info("Checkbox is checked " + checked);
        return checked;
    }
}
