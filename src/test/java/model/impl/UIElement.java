package model.impl;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.ReadTestDataFile;

import java.util.List;

@Log4j
public abstract class UIElement {

    private By by;
    private WebElement element;
    private WebDriverWait wait;
    private WebDriver driver;

    protected ReadTestDataFile readTestDataFile = new ReadTestDataFile();

    ////////////////////
    /// Constructors ///
    ////////////////////

    public UIElement(WebDriver driver, By by) {
        this.by = by;
        this.driver = driver;
        // ToDo: Time can be handled from property file
        this.wait = new WebDriverWait(driver, 30);
    }

    protected WebElement getElement() {
        return element == null ? driver.findElement(by) : element;
    }

    protected List<WebElement> getElements() {
        return driver.findElements(by);
    }

    //////////////
    /// Waits ///
    /////////////

    protected void waitForElementVisible() {
        wait.until(ExpectedConditions.visibilityOf(getElement()));
    }

    public void waitForElementPresents() {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    protected void waitForElementClickable() {
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    protected void waitForAllElementsArePresents(int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    protected void waitForAllElementsAreVisible(int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }

    public void waitForElementInvisible() {
        try {
            Thread.sleep(300);
        } catch (Exception ignored) {
        }
        if (isPresents()) {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
        }
    }

    // ToDo: Need to find the solution for jQuery wait

    /**
     * Method to wait until page will be fully loaded
     */
    protected void waitPageIsLoaded() {
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor js = (JavascriptExecutor) driver;
//                String jQueryState = (Boolean) js.executeScript("return window.jQuery != undefined") ? js.executeScript("return window.jQuery.active").toString() : "undefined";
                String documentState = (String) js.executeScript("return document.readyState");
//                LOGGER.info("jQuery state is: " + jQueryState);
                log.info("Document state is: " + documentState);
//                return jQueryState.equals("0") && documentState.equals("complete");
                return documentState.equals("complete");
            }
        });
    }

    public void waitUntilElementAttributeToBe(String attribute, String value) {
        wait.until(ExpectedConditions.attributeToBe(getElement(), attribute, value));
    }

    public void waitUntilElementAttributeToContain(String attribute, String value) {
        wait.until(ExpectedConditions.attributeContains(getElement(), attribute, value));
    }

    protected void waitEverythingIsLoaded() {
        waitPageIsLoaded();
        waitForElementPresents();
        waitForElementVisible();
    }

    public void waitElementIsEnabled() {
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return isEnabled();
            }
        });
    }

    public void waitUntilElementCountToBeMore(int elementCountToBe) {
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(by, elementCountToBe));
    }

    public void waitUntilElementCountToBeLess(int elementCountToBe) {
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(by, elementCountToBe));
    }

    ///////////////
    /// Actions ///
    ///////////////

    protected void moveToElement() {
        Actions actions = new Actions(driver);
        actions.moveToElement(getElement()).perform();
    }

    // ToDo need to remove unused methods
    protected void waitAndClick() {
        waitEverythingIsLoaded();
        waitForElementVisible();
        scrollToElement();
        scrollAndClick();
        waitPageIsLoaded();
    }

    protected void fillTextBox(String input) {
        waitEverythingIsLoaded();
        moveToElement();
        // ToDo temporary solution due to validation error message is not properly deisplayed when using clear method
        if (input.isEmpty()) {
            getElement().sendKeys(Keys.SPACE);
            getElement().sendKeys(Keys.CONTROL, "a");
            getElement().sendKeys(Keys.DELETE);
        } else {
            getElement().clear();
            getElement().sendKeys(readTestDataFile.getDataFromFile(input));
        }
        waitPageIsLoaded();
    }

    /**
     * Global method. Used for every web element.
     *
     * @return String
     */
    public String getValue() {
        waitEverythingIsLoaded();
        moveToElement();
        String result = getElement().getText();
        log.info("Text content from UI: " + result);
        return result;
    }

    public String getAttribute(String attr) {
        waitPageIsLoaded();
        waitForElementPresents();
        String value = getElement().getAttribute(attr).trim();
        log.info("Attribute: '" + attr + "', value is '" + value + "'");
        return value;
    }

    private void scrollToElement() {
        JavascriptExecutor je = (JavascriptExecutor) driver;
        long windowHeight = (Long) je.executeScript("return window.innerHeight;");
        log.info("Inner height " + windowHeight);
        // Element position before scroll
        Point p1 = getElement().getLocation();
        je.executeScript("arguments[0].scrollIntoView();", getElement());
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Element position after scroll
        Point p2 = getElement().getLocation();
        if (p2.getY() - p1.getY() > 200) {
            je.executeScript("window.scrollBy(0, " + (-windowHeight / 4) + ");");
        }
    }

    private void scrollAndClick() {
        boolean clickable = false;
        JavascriptExecutor je = (JavascriptExecutor) driver;
        for (int i = 0; i < 5; i++) {
            try {
                getElement().click();
                clickable = true;
                return;
            } catch (Exception e) {
                log.info("Element is not clickable");
                je.executeScript("window.scrollBy(0, -50)");
            }
        }
    }

    public void clickOutsideElement(int x, int y) {
        waitPageIsLoaded();
        waitForElementPresents();
        Actions builder = new Actions(driver);
        builder.moveToElement(getElement(), x, y).click().build().perform();
    }

    ///////////////////////////
    /// Change web element  ///
    ///////////////////////////

    protected WebElement getFollowingSibling() {
        return getElement().findElement(By.xpath("//following-sibling::*"));
    }

    protected WebElement getParentElement() {
        return getElement().findElement(By.xpath(".."));
    }

    protected WebElement getPrecedingSibling() {
        return getElement().findElement(By.xpath("/preceding-sibling::*[1]"));
    }

    ////////////////
    /// Booleans ///
    ////////////////

    public boolean isDisplayed() {
        waitPageIsLoaded();
        waitForElementPresents();
        boolean isDisplayed = getElement().isDisplayed();
        log.info("Element is displayed " + isDisplayed);
        return isDisplayed;
    }

    public boolean isPresents() {
        waitPageIsLoaded();
        int elementCount = driver.findElements(by).size();
        return elementCount > 0;
    }

    public boolean isEnabled() {
        waitPageIsLoaded();
        waitForElementPresents();
        return getElement().isEnabled();
    }

    public boolean isNotDisplayed() {
        boolean isNotVisible;
        isNotVisible = !isPresents() || !isDisplayed();
        log.info("Element is not displayed " + isNotVisible);
        return isNotVisible;
    }

}
