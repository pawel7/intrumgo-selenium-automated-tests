package model.impl;

import lombok.extern.log4j.Log4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Log4j
public class ElementList extends UIElement {
    public ElementList(WebDriver driver, By by) {
        super(driver, by);
    }

    public int getItemCount() {
        waitPageIsLoaded();
        int elementCount = 0;
        try {
            waitForAllElementsArePresents(1);
            waitForAllElementsAreVisible(1);
            moveToElement();
            elementCount = getElements().size();
        } catch (Exception ignored) {
        }
        log.info("Element count is " + elementCount);
        return elementCount;
    }

    @Override
    public String getValue() {
        log.error("Value is not defined");
        return null;
    }
}
