package model.impl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RadioButton extends UIElement {
    public RadioButton(WebDriver driver, By by) {
        super(driver, by);
    }

    public void click() {
        waitAndClick();
    }

    public boolean isChecked() {
        waitEverythingIsLoaded();
        waitPageIsLoaded();
        return getElement().getAttribute("checked").equals("true");
    }

}
