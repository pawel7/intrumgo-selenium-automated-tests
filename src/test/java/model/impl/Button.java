package model.impl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Button extends UIElement {
    public Button(WebDriver driver, By by) {
        super(driver, by);
    }

    public void click() {
        waitAndClick();
    }

    public void waitForVisibility() {
        waitForElementPresents();
        waitForElementVisible();
    }

}
